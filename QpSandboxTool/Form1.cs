﻿// Decompiled with JetBrains decompiler
// Type: QpSandboxTool.Form1
// Assembly: QpSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5335FC56-AF03-4496-80B0-F5105898AB82
// Assembly location: C:\SWAN\MEA\QP Sandbox Tool 2012 08 31\QP Sandbox Tool\QpSandboxTool.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace QpSandboxTool
{
  public class Form1 : Form
  {
    private IContainer components = (IContainer) null;
    private TextBox vehLpnTextBox;
    private Label label5;
    private Button queryByVehicleButton;
    private TextBox urlTextBox;
    private Label label3;
    private Button helpButton;
    private TextBox tokenTextBox;
    private Label label4;
    private TextBox locIdTextBox;
    private Label label2;
    private TextBox custIdTextBox;
    private Label label1;
    private ListBox listBox1;
    private Button queryByZoneButton;
    private TextBox enfIdTextBox;
    private Label label6;
    private TextBox vehStateTextBox;
    private Label label7;
    private TextBox stallStartTextBox;
    private Label label8;
    private TextBox stallEndTextBox;
    private Label label9;
    private CheckBox usePostCheckBox;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.vehLpnTextBox = new TextBox();
      this.label5 = new Label();
      this.queryByVehicleButton = new Button();
      this.urlTextBox = new TextBox();
      this.label3 = new Label();
      this.helpButton = new Button();
      this.tokenTextBox = new TextBox();
      this.label4 = new Label();
      this.locIdTextBox = new TextBox();
      this.label2 = new Label();
      this.custIdTextBox = new TextBox();
      this.label1 = new Label();
      this.listBox1 = new ListBox();
      this.queryByZoneButton = new Button();
      this.enfIdTextBox = new TextBox();
      this.label6 = new Label();
      this.vehStateTextBox = new TextBox();
      this.label7 = new Label();
      this.stallStartTextBox = new TextBox();
      this.label8 = new Label();
      this.stallEndTextBox = new TextBox();
      this.label9 = new Label();
      this.usePostCheckBox = new CheckBox();
      this.SuspendLayout();
      this.vehLpnTextBox.Location = new Point(641, 14);
      this.vehLpnTextBox.Margin = new Padding(4);
      this.vehLpnTextBox.Name = "vehLpnTextBox";
      this.vehLpnTextBox.Size = new Size(143, 22);
      this.vehLpnTextBox.TabIndex = 57;
      this.label5.AutoSize = true;
      this.label5.Location = new Point(544, 18);
      this.label5.Margin = new Padding(4, 0, 4, 0);
      this.label5.Name = "label5";
      this.label5.Size = new Size(89, 17);
      this.label5.TabIndex = 56;
      this.label5.Text = "Vehicle LPN:";
      this.queryByVehicleButton.Location = new Point(347, 590);
      this.queryByVehicleButton.Margin = new Padding(4);
      this.queryByVehicleButton.Name = "queryByVehicleButton";
      this.queryByVehicleButton.Size = new Size(271, 28);
      this.queryByVehicleButton.TabIndex = 55;
      this.queryByVehicleButton.Text = "Query Parking Rights by Vehicle";
      this.queryByVehicleButton.UseVisualStyleBackColor = true;
      this.queryByVehicleButton.Click += new EventHandler(this.queryByVehicleButton_Click);
      this.urlTextBox.Location = new Point(602, 48);
      this.urlTextBox.Margin = new Padding(4);
      this.urlTextBox.Name = "urlTextBox";
      this.urlTextBox.Size = new Size(471, 22);
      this.urlTextBox.TabIndex = 54;
      this.label3.AutoSize = true;
      this.label3.Location = new Point(538, 51);
      this.label3.Margin = new Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new Size(40, 17);
      this.label3.TabIndex = 53;
      this.label3.Text = "URL:";
      this.helpButton.Location = new Point(1066, 590);
      this.helpButton.Margin = new Padding(4);
      this.helpButton.Name = "helpButton";
      this.helpButton.Size = new Size(112, 28);
      this.helpButton.TabIndex = 52;
      this.helpButton.Text = "Help";
      this.helpButton.UseVisualStyleBackColor = true;
      this.helpButton.Click += new EventHandler(this.helpButton_Click);
      this.tokenTextBox.Location = new Point(108, 47);
      this.tokenTextBox.Margin = new Padding(4);
      this.tokenTextBox.Name = "tokenTextBox";
      this.tokenTextBox.Size = new Size(366, 22);
      this.tokenTextBox.TabIndex = 51;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(15, 51);
      this.label4.Margin = new Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new Size(88, 17);
      this.label4.TabIndex = 50;
      this.label4.Text = "Token (key):";
      this.locIdTextBox.Location = new Point(359, 15);
      this.locIdTextBox.Margin = new Padding(4);
      this.locIdTextBox.Name = "locIdTextBox";
      this.locIdTextBox.Size = new Size(145, 22);
      this.locIdTextBox.TabIndex = 49;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(268, 19);
      this.label2.Margin = new Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new Size(83, 17);
      this.label2.TabIndex = 48;
      this.label2.Text = "Location ID:";
      this.custIdTextBox.Location = new Point(108, 15);
      this.custIdTextBox.Margin = new Padding(4);
      this.custIdTextBox.Name = "custIdTextBox";
      this.custIdTextBox.Size = new Size(143, 22);
      this.custIdTextBox.TabIndex = 47;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(15, 19);
      this.label1.Margin = new Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(89, 17);
      this.label1.TabIndex = 46;
      this.label1.Text = "Customer ID:";
      this.listBox1.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.listBox1.FormattingEnabled = true;
      this.listBox1.HorizontalScrollbar = true;
      this.listBox1.ItemHeight = 25;
      this.listBox1.Location = new Point(19, 109);
      this.listBox1.Margin = new Padding(4);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(1162, 479);
      this.listBox1.TabIndex = 45;
      this.queryByZoneButton.Location = new Point(19, 590);
      this.queryByZoneButton.Margin = new Padding(4);
      this.queryByZoneButton.Name = "queryByZoneButton";
      this.queryByZoneButton.Size = new Size(271, 28);
      this.queryByZoneButton.TabIndex = 44;
      this.queryByZoneButton.Text = "Query Parking Rights by Zone";
      this.queryByZoneButton.UseVisualStyleBackColor = true;
      this.queryByZoneButton.Click += new EventHandler(this.queryByZoneButton_Click);
      this.enfIdTextBox.Location = new Point(108, 75);
      this.enfIdTextBox.Margin = new Padding(4);
      this.enfIdTextBox.Name = "enfIdTextBox";
      this.enfIdTextBox.Size = new Size(143, 22);
      this.enfIdTextBox.TabIndex = 59;
      this.label6.AutoSize = true;
      this.label6.Location = new Point(15, 79);
      this.label6.Margin = new Padding(4, 0, 4, 0);
      this.label6.Name = "label6";
      this.label6.Size = new Size(83, 17);
      this.label6.TabIndex = 58;
      this.label6.Text = "Enforcer ID:";
      this.vehStateTextBox.Location = new Point(901, 13);
      this.vehStateTextBox.Margin = new Padding(4);
      this.vehStateTextBox.Name = "vehStateTextBox";
      this.vehStateTextBox.Size = new Size(55, 22);
      this.vehStateTextBox.TabIndex = 61;
      this.label7.AutoSize = true;
      this.label7.Location = new Point(804, 17);
      this.label7.Margin = new Padding(4, 0, 4, 0);
      this.label7.Name = "label7";
      this.label7.Size = new Size(95, 17);
      this.label7.TabIndex = 60;
      this.label7.Text = "Vehicle State:";
      this.stallStartTextBox.Location = new Point(395, 76);
      this.stallStartTextBox.Margin = new Padding(4);
      this.stallStartTextBox.Name = "stallStartTextBox";
      this.stallStartTextBox.Size = new Size(143, 22);
      this.stallStartTextBox.TabIndex = 63;
      this.label8.AutoSize = true;
      this.label8.Location = new Point(296, 80);
      this.label8.Margin = new Padding(4, 0, 4, 0);
      this.label8.Name = "label8";
      this.label8.Size = new Size(92, 17);
      this.label8.TabIndex = 62;
      this.label8.Text = "Starting Stall:";
      this.stallEndTextBox.Location = new Point(667, 76);
      this.stallEndTextBox.Margin = new Padding(4);
      this.stallEndTextBox.Name = "stallEndTextBox";
      this.stallEndTextBox.Size = new Size(143, 22);
      this.stallEndTextBox.TabIndex = 65;
      this.label9.AutoSize = true;
      this.label9.Location = new Point(568, 80);
      this.label9.Margin = new Padding(4, 0, 4, 0);
      this.label9.Name = "label9";
      this.label9.Size = new Size(87, 17);
      this.label9.TabIndex = 64;
      this.label9.Text = "Ending Stall:";
      this.usePostCheckBox.AutoSize = true;
      this.usePostCheckBox.Location = new Point(834, 78);
      this.usePostCheckBox.Name = "usePostCheckBox";
      this.usePostCheckBox.Size = new Size(138, 21);
      this.usePostCheckBox.TabIndex = 66;
      this.usePostCheckBox.Text = "Use HTTP POST";
      this.usePostCheckBox.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(1197, 633);
      this.Controls.Add((Control) this.usePostCheckBox);
      this.Controls.Add((Control) this.stallEndTextBox);
      this.Controls.Add((Control) this.label9);
      this.Controls.Add((Control) this.stallStartTextBox);
      this.Controls.Add((Control) this.label8);
      this.Controls.Add((Control) this.vehStateTextBox);
      this.Controls.Add((Control) this.label7);
      this.Controls.Add((Control) this.enfIdTextBox);
      this.Controls.Add((Control) this.label6);
      this.Controls.Add((Control) this.vehLpnTextBox);
      this.Controls.Add((Control) this.label5);
      this.Controls.Add((Control) this.queryByVehicleButton);
      this.Controls.Add((Control) this.urlTextBox);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.helpButton);
      this.Controls.Add((Control) this.tokenTextBox);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.locIdTextBox);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.custIdTextBox);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.listBox1);
      this.Controls.Add((Control) this.queryByZoneButton);
      this.Name = nameof (Form1);
      this.Text = "QP Sandbox Tool";
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Form1()
    {
      this.InitializeComponent();
      this.urlTextBox.Text = "http://69.169.142.26:8080/geopay/api/enforcement";
      this.custIdTextBox.Text = "5";
      this.locIdTextBox.Text = "1234";
      this.enfIdTextBox.Text = "blue52";
      this.vehLpnTextBox.Text = "30AUG12";
      this.vehStateTextBox.Text = "UT";
      this.stallStartTextBox.Text = "";
      this.stallEndTextBox.Text = "";
      this.tokenTextBox.Text = "t2Key:b2773cc0-d052-11e1-9b23-0800200c9a66";
    }

    private void queryByZoneButton_Click(object sender, EventArgs e)
    {
      string text = this.urlTextBox.Text;
      string parameters = "CustomerId=" + this.custIdTextBox.Text + "&LocationId=" + this.locIdTextBox.Text + "&EnforcerId=" + this.enfIdTextBox.Text + "&Key=" + this.tokenTextBox.Text;
      if (this.stallStartTextBox.Text.Length > 0)
        parameters = parameters + "&StallStart=" + this.stallStartTextBox.Text;
      if (this.stallEndTextBox.Text.Length > 0)
        parameters = parameters + "&StallEnd=" + this.stallEndTextBox.Text;
      this.listBox1.Items.Add((object) "Query by Location:");
      this.listBox1.Items.Add((object) text);
      this.listBox1.Items.Add((object) parameters);
      string str1 = !this.usePostCheckBox.Checked ? this.httpGet(text + "?" + parameters, "") : this.httpPost(text, parameters, -1, "");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "Responce:");
      string str2 = "enforcementResponse";
      int startIndex1 = str1.IndexOf("<" + str2);
      if (startIndex1 >= 0)
        str1 = str1.Substring(startIndex1);
      else
        this.listBox1.Items.Add((object) str1);
      while (true)
      {
        int startIndex2 = str1.IndexOf("<" + str2);
        int num1 = str1.IndexOf("</" + str2);
        if (startIndex2 >= 0 && num1 >= 0)
        {
          string theXML = str1.Substring(startIndex2, num1 - startIndex2);
          string theZone;
          DateTime dateTimeStyleV1 = Form1.parseDateTimeStyleV(Form1.parseXMLtag(ref theXML, "purchaseDate"), out theZone);
          dateTimeStyleV1 += TimeZone.CurrentTimeZone.GetUtcOffset(dateTimeStyleV1);
          DateTime dateTimeStyleV2 = Form1.parseDateTimeStyleV(Form1.parseXMLtag(ref theXML, "sessionExpiration"), out theZone);
          dateTimeStyleV2 += TimeZone.CurrentTimeZone.GetUtcOffset(dateTimeStyleV2);
          ListBox.ObjectCollection items = this.listBox1.Items;
          string[] strArray1 = new string[28];
          strArray1[0] = "LPN:";
          strArray1[1] = Form1.parseXMLtag(ref theXML, "licensePlate");
          strArray1[2] = " ";
          strArray1[3] = Form1.parseXMLtag(ref theXML, "licenseState");
          strArray1[4] = " Loc:";
          strArray1[5] = Form1.parseXMLtag(ref theXML, "locationID");
          strArray1[6] = " Space:";
          strArray1[7] = Form1.parseXMLtag(ref theXML, "startSpace");
          strArray1[8] = " Purchase: ";
          string[] strArray2 = strArray1;
          int num2 = dateTimeStyleV1.Month;
          string str3 = num2.ToString("D2");
          strArray2[9] = str3;
          strArray1[10] = "/";
          string[] strArray3 = strArray1;
          num2 = dateTimeStyleV1.Day;
          string str4 = num2.ToString("D2");
          strArray3[11] = str4;
          strArray1[12] = " ";
          string[] strArray4 = strArray1;
          num2 = dateTimeStyleV1.Hour;
          string str5 = num2.ToString("D2");
          strArray4[13] = str5;
          strArray1[14] = ":";
          string[] strArray5 = strArray1;
          num2 = dateTimeStyleV1.Minute;
          string str6 = num2.ToString("D2");
          strArray5[15] = str6;
          strArray1[16] = ":";
          string[] strArray6 = strArray1;
          num2 = dateTimeStyleV1.Second;
          string str7 = num2.ToString("D2");
          strArray6[17] = str7;
          strArray1[18] = " Exp: ";
          string[] strArray7 = strArray1;
          num2 = dateTimeStyleV2.Month;
          string str8 = num2.ToString("D2");
          strArray7[19] = str8;
          strArray1[20] = "/";
          string[] strArray8 = strArray1;
          num2 = dateTimeStyleV2.Day;
          string str9 = num2.ToString("D2");
          strArray8[21] = str9;
          strArray1[22] = " ";
          string[] strArray9 = strArray1;
          num2 = dateTimeStyleV2.Hour;
          string str10 = num2.ToString("D2");
          strArray9[23] = str10;
          strArray1[24] = ":";
          string[] strArray10 = strArray1;
          num2 = dateTimeStyleV2.Minute;
          string str11 = num2.ToString("D2");
          strArray10[25] = str11;
          strArray1[26] = ":";
          string[] strArray11 = strArray1;
          num2 = dateTimeStyleV2.Second;
          string str12 = num2.ToString("D2");
          strArray11[27] = str12;
          string str13 = string.Concat(strArray1);
          items.Add((object) str13);
          str1 = str1.Substring(num1 + str2.Length + 3);
        }
        else
          break;
      }
      this.listBox1.Items.Add((object) "");
    }

    private void queryByVehicleButton_Click(object sender, EventArgs e)
    {
      string text = this.urlTextBox.Text;
      string parameters = "CustomerId=" + this.custIdTextBox.Text + "&EnforcerId=" + this.enfIdTextBox.Text + "&Key=" + this.tokenTextBox.Text + "&LicensePlate=" + this.vehLpnTextBox.Text;
      this.listBox1.Items.Add((object) "Query by Vehicle:");
      this.listBox1.Items.Add((object) text);
      this.listBox1.Items.Add((object) parameters);
      string str1 = !this.usePostCheckBox.Checked ? this.httpGet(text + "?" + parameters, "") : this.httpPost(text, parameters, -1, "");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "Responce:");
      string str2 = "enforcementResponse";
      int startIndex1 = str1.IndexOf("<" + str2);
      if (startIndex1 >= 0)
        str1 = str1.Substring(startIndex1);
      else
        this.listBox1.Items.Add((object) str1);
      while (true)
      {
        int startIndex2 = str1.IndexOf("<" + str2);
        int num1 = str1.IndexOf("</" + str2);
        if (startIndex2 >= 0 && num1 >= 0)
        {
          string theXML = str1.Substring(startIndex2, num1 - startIndex2);
          string theZone;
          DateTime dateTimeStyleV1 = Form1.parseDateTimeStyleV(Form1.parseXMLtag(ref theXML, "purchaseDate"), out theZone);
          DateTime dateTime = dateTimeStyleV1 + TimeZone.CurrentTimeZone.GetUtcOffset(dateTimeStyleV1);
          DateTime dateTimeStyleV2 = Form1.parseDateTimeStyleV(Form1.parseXMLtag(ref theXML, "sessionExpiration"), out theZone);
          dateTimeStyleV2 += TimeZone.CurrentTimeZone.GetUtcOffset(dateTimeStyleV2);
          ListBox.ObjectCollection items = this.listBox1.Items;
          string[] strArray1 = new string[28];
          strArray1[0] = "LPN:";
          strArray1[1] = Form1.parseXMLtag(ref theXML, "licensePlate");
          strArray1[2] = " ";
          strArray1[3] = Form1.parseXMLtag(ref theXML, "licenseState");
          strArray1[4] = " Loc:";
          strArray1[5] = Form1.parseXMLtag(ref theXML, "locationID");
          strArray1[6] = " Space:";
          strArray1[7] = Form1.parseXMLtag(ref theXML, "startSpace");
          strArray1[8] = " Purchase: ";
          string[] strArray2 = strArray1;
          int num2 = dateTime.Month;
          string str3 = num2.ToString("D2");
          strArray2[9] = str3;
          strArray1[10] = "/";
          string[] strArray3 = strArray1;
          num2 = dateTime.Day;
          string str4 = num2.ToString("D2");
          strArray3[11] = str4;
          strArray1[12] = " ";
          string[] strArray4 = strArray1;
          num2 = dateTime.Hour;
          string str5 = num2.ToString("D2");
          strArray4[13] = str5;
          strArray1[14] = ":";
          string[] strArray5 = strArray1;
          num2 = dateTime.Minute;
          string str6 = num2.ToString("D2");
          strArray5[15] = str6;
          strArray1[16] = ":";
          string[] strArray6 = strArray1;
          num2 = dateTime.Second;
          string str7 = num2.ToString("D2");
          strArray6[17] = str7;
          strArray1[18] = " Exp: ";
          string[] strArray7 = strArray1;
          num2 = dateTimeStyleV2.Month;
          string str8 = num2.ToString("D2");
          strArray7[19] = str8;
          strArray1[20] = "/";
          string[] strArray8 = strArray1;
          num2 = dateTimeStyleV2.Day;
          string str9 = num2.ToString("D2");
          strArray8[21] = str9;
          strArray1[22] = " ";
          string[] strArray9 = strArray1;
          num2 = dateTimeStyleV2.Hour;
          string str10 = num2.ToString("D2");
          strArray9[23] = str10;
          strArray1[24] = ":";
          string[] strArray10 = strArray1;
          num2 = dateTimeStyleV2.Minute;
          string str11 = num2.ToString("D2");
          strArray10[25] = str11;
          strArray1[26] = ":";
          string[] strArray11 = strArray1;
          num2 = dateTimeStyleV2.Second;
          string str12 = num2.ToString("D2");
          strArray11[27] = str12;
          string str13 = string.Concat(strArray1);
          items.Add((object) str13);
          str1 = str1.Substring(num1 + str2.Length + 3);
        }
        else
          break;
      }
      this.listBox1.Items.Add((object) "");
    }

    private void helpButton_Click(object sender, EventArgs e)
    {
    }

    private string httpPost(string theUri, string parameters, int timeOut, string soapAction)
    {
      try
      {
        ServicePointManager.CertificatePolicy = (ICertificatePolicy) new Form1.TrustAllCertificatePolicy();
        WebRequest webRequest = WebRequest.Create(theUri);
        webRequest.ContentType = "application/x-www-form-urlencoded";
        webRequest.Method = "POST";
        if (soapAction.Length > 0)
          webRequest.Headers.Add("SOAPAction", soapAction);
        if (timeOut > 0)
          webRequest.Timeout = timeOut;
        byte[] bytes = Encoding.ASCII.GetBytes(parameters);
        webRequest.ContentLength = (long) bytes.Length;
        Stream requestStream = webRequest.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();
        WebResponse response;
        try
        {
          response = webRequest.GetResponse();
        }
        catch (WebException ex)
        {
          if (ex.Response != null)
          {
            StreamReader streamReader = new StreamReader(ex.Response.GetResponseStream());
            this.logException("httpPost() - " + streamReader.ReadToEnd(), (Exception) ex);
            streamReader.Close();
          }
          throw;
        }
        if (response == null)
          return "";
        StreamReader streamReader1 = new StreamReader(response.GetResponseStream());
        string str = streamReader1.ReadToEnd().Trim();
        streamReader1.Close();
        return str.Replace("&lt;", "<").Replace("&gt;", ">");
      }
      catch (Exception ex)
      {
        this.logException("httpPost() ERROR", ex);
        return "ERROR";
      }
    }

    public string httpGet(string theUri, string parameters)
    {
      ServicePointManager.CertificatePolicy = (ICertificatePolicy) new Form1.TrustAllCertificatePolicy();
      WebRequest webRequest = WebRequest.Create(theUri);
      webRequest.ContentType = "application/x-www-form-urlencoded";
      webRequest.Method = "GET";
      try
      {
        WebResponse response = webRequest.GetResponse();
        if (response == null)
          return "";
        StreamReader streamReader = new StreamReader(response.GetResponseStream());
        string str = streamReader.ReadToEnd().Trim();
        streamReader.Close();
        return str.Replace("&lt;", "<").Replace("&gt;", ">");
      }
      catch (WebException ex)
      {
        if (ex.Response != null)
        {
          StreamReader streamReader = new StreamReader(ex.Response.GetResponseStream());
          this.logException("httpGet() - " + streamReader.ReadToEnd(), (Exception) ex);
          streamReader.Close();
        }
        this.logException("httpGet() Error", (Exception) ex);
        return "";
      }
    }

    private void logException(string theMessage, Exception ex)
    {
      this.listBox1.Items.Add((object) theMessage);
      if (ex == null)
        return;
      this.listBox1.Items.Add((object) ex.Message);
      if (ex.InnerException != null)
        this.listBox1.Items.Add((object) ("Inner Exception: " + ex.InnerException.Message));
      this.listBox1.Items.Add((object) ("Stack Trace: " + ex.StackTrace));
    }

    internal static string parseXMLtag(ref string theXML, string theTag)
    {
      string str1 = "<" + theTag + ">";
      string str2 = "</" + theTag + ">";
      int num1 = theXML.IndexOf(str1);
      int num2 = theXML.IndexOf(str2);
      return num1 >= 0 && num2 > num1 ? theXML.Substring(num1 + str1.Length, num2 - num1 - str1.Length) : "";
    }

    internal static DateTime parseDateTimeStyleV(string dateTime, out string theZone)
    {
      DateTime dateTime1 = DateTime.Now;
      theZone = "XXX";
      try
      {
        int num1 = dateTime.IndexOf('-');
        int int16_1 = (int) Convert.ToInt16(dateTime.Substring(num1 - 4, 4));
        int int16_2 = (int) Convert.ToInt16(dateTime.Substring(num1 + 1, 2));
        int int16_3 = (int) Convert.ToInt16(dateTime.Substring(num1 + 4, 2));
        int num2 = dateTime.IndexOf(':');
        int int16_4 = (int) Convert.ToInt16(dateTime.Substring(num2 - 2, 2));
        int int16_5 = (int) Convert.ToInt16(dateTime.Substring(num2 + 1, 2));
        int int16_6 = (int) Convert.ToInt16(dateTime.Substring(num2 + 4, 2));
        theZone = dateTime.Substring(num2 + 7, 3);
        dateTime1 = new DateTime(int16_1, int16_2, int16_3, int16_4, int16_5, int16_6);
      }
      catch
      {
      }
      return dateTime1;
    }

    public class TrustAllCertificatePolicy : ICertificatePolicy
    {
      public bool CheckValidationResult(
        ServicePoint sp,
        X509Certificate cert,
        WebRequest req,
        int problem)
      {
        return true;
      }
    }
  }
}
