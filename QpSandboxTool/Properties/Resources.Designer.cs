﻿// Decompiled with JetBrains decompiler
// Type: QpSandboxTool.Properties.Resources
// Assembly: QpSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5335FC56-AF03-4496-80B0-F5105898AB82
// Assembly location: C:\SWAN\MEA\QP Sandbox Tool 2012 08 31\QP Sandbox Tool\QpSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace QpSandboxTool.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [CompilerGenerated]
  [DebuggerNonUserCode]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) QpSandboxTool.Properties.Resources.resourceMan, (object) null))
          QpSandboxTool.Properties.Resources.resourceMan = new ResourceManager("QpSandboxTool.Properties.Resources", typeof (QpSandboxTool.Properties.Resources).Assembly);
        return QpSandboxTool.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return QpSandboxTool.Properties.Resources.resourceCulture;
      }
      set
      {
        QpSandboxTool.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
