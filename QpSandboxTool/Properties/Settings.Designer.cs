﻿// Decompiled with JetBrains decompiler
// Type: QpSandboxTool.Properties.Settings
// Assembly: QpSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5335FC56-AF03-4496-80B0-F5105898AB82
// Assembly location: C:\SWAN\MEA\QP Sandbox Tool 2012 08 31\QP Sandbox Tool\QpSandboxTool.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace QpSandboxTool.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        Settings defaultInstance = Settings.defaultInstance;
        return defaultInstance;
      }
    }
  }
}
