﻿// Decompiled with JetBrains decompiler
// Type: QpSandboxTool.Program
// Assembly: QpSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5335FC56-AF03-4496-80B0-F5105898AB82
// Assembly location: C:\SWAN\MEA\QP Sandbox Tool 2012 08 31\QP Sandbox Tool\QpSandboxTool.exe

using System;
using System.Windows.Forms;

namespace QpSandboxTool
{
  internal static class Program
  {
    [STAThread]
    private static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run((Form) new Form1());
    }
  }
}
