﻿// Decompiled with JetBrains decompiler
// Type: MobileNowSandboxTool.Properties.Resources
// Assembly: MobileNowSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EB0F4647-E969-4CC7-A7D8-1F9B8A31E47D
// Assembly location: C:\SWAN\MEA\MobileNow Sandbox Tool 2012 07 19B\MobileNow Sandbox Tool\MobileNowSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace MobileNowSandboxTool.Properties
{
  [DebuggerNonUserCode]
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (MobileNowSandboxTool.Properties.Resources.resourceMan == null)
          MobileNowSandboxTool.Properties.Resources.resourceMan = new ResourceManager("MobileNowSandboxTool.Properties.Resources", typeof (MobileNowSandboxTool.Properties.Resources).Assembly);
        return MobileNowSandboxTool.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return MobileNowSandboxTool.Properties.Resources.resourceCulture;
      }
      set
      {
        MobileNowSandboxTool.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
