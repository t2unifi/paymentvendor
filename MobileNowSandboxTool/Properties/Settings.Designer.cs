﻿// Decompiled with JetBrains decompiler
// Type: MobileNowSandboxTool.Properties.Settings
// Assembly: MobileNowSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EB0F4647-E969-4CC7-A7D8-1F9B8A31E47D
// Assembly location: C:\SWAN\MEA\MobileNow Sandbox Tool 2012 07 19B\MobileNow Sandbox Tool\MobileNowSandboxTool.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace MobileNowSandboxTool.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        Settings defaultInstance = Settings.defaultInstance;
        return defaultInstance;
      }
    }
  }
}
