﻿// Decompiled with JetBrains decompiler
// Type: MobileNowSandboxTool.Form1
// Assembly: MobileNowSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EB0F4647-E969-4CC7-A7D8-1F9B8A31E47D
// Assembly location: C:\SWAN\MEA\MobileNow Sandbox Tool 2012 07 19B\MobileNow Sandbox Tool\MobileNowSandboxTool.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace MobileNowSandboxTool
{
  public class Form1 : Form
  {
    private IContainer components = (IContainer) null;
    private Label label3;
    private Button helpButton;
    private TextBox passwordTextBox;
    private Label label4;
    private TextBox zoneCodeTextBox;
    private Label label2;
    private TextBox userIdTextBox;
    private Label label1;
    private ListBox listBox1;
    private Button queryByZoneButton;
    private TextBox urlTextBox;
    private Button queryByVehicleButton;
    private TextBox vehTextBox;
    private Label label5;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.label3 = new Label();
      this.helpButton = new Button();
      this.passwordTextBox = new TextBox();
      this.label4 = new Label();
      this.zoneCodeTextBox = new TextBox();
      this.label2 = new Label();
      this.userIdTextBox = new TextBox();
      this.label1 = new Label();
      this.listBox1 = new ListBox();
      this.queryByZoneButton = new Button();
      this.urlTextBox = new TextBox();
      this.queryByVehicleButton = new Button();
      this.vehTextBox = new TextBox();
      this.label5 = new Label();
      this.SuspendLayout();
      this.label3.AutoSize = true;
      this.label3.Location = new Point(369, 66);
      this.label3.Margin = new Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new Size(40, 17);
      this.label3.TabIndex = 39;
      this.label3.Text = "URL:";
      this.helpButton.Location = new Point(1052, 605);
      this.helpButton.Margin = new Padding(4);
      this.helpButton.Name = "helpButton";
      this.helpButton.Size = new Size(112, 28);
      this.helpButton.TabIndex = 36;
      this.helpButton.Text = "Help";
      this.helpButton.UseVisualStyleBackColor = true;
      this.helpButton.Click += new EventHandler(this.helpButton_Click);
      this.passwordTextBox.Location = new Point(92, 62);
      this.passwordTextBox.Margin = new Padding(4);
      this.passwordTextBox.Name = "passwordTextBox";
      this.passwordTextBox.Size = new Size(143, 22);
      this.passwordTextBox.TabIndex = 35;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(1, 66);
      this.label4.Margin = new Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new Size(73, 17);
      this.label4.TabIndex = 34;
      this.label4.Text = "Password:";
      this.zoneCodeTextBox.Location = new Point(433, 30);
      this.zoneCodeTextBox.Margin = new Padding(4);
      this.zoneCodeTextBox.Name = "zoneCodeTextBox";
      this.zoneCodeTextBox.Size = new Size(145, 22);
      this.zoneCodeTextBox.TabIndex = 33;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(254, 34);
      this.label2.Margin = new Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new Size(175, 17);
      this.label2.TabIndex = 32;
      this.label2.Text = "Zone Code (Control Area):";
      this.userIdTextBox.Location = new Point(92, 30);
      this.userIdTextBox.Margin = new Padding(4);
      this.userIdTextBox.Name = "userIdTextBox";
      this.userIdTextBox.Size = new Size(143, 22);
      this.userIdTextBox.TabIndex = 31;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(1, 34);
      this.label1.Margin = new Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(59, 17);
      this.label1.TabIndex = 30;
      this.label1.Text = "User ID:";
      this.listBox1.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.listBox1.FormattingEnabled = true;
      this.listBox1.ItemHeight = 25;
      this.listBox1.Location = new Point(5, 124);
      this.listBox1.Margin = new Padding(4);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(1162, 479);
      this.listBox1.TabIndex = 29;
      this.queryByZoneButton.Location = new Point(5, 605);
      this.queryByZoneButton.Margin = new Padding(4);
      this.queryByZoneButton.Name = "queryByZoneButton";
      this.queryByZoneButton.Size = new Size(271, 28);
      this.queryByZoneButton.TabIndex = 28;
      this.queryByZoneButton.Text = "Query Parking Rights by Zone";
      this.queryByZoneButton.UseVisualStyleBackColor = true;
      this.queryByZoneButton.Click += new EventHandler(this.queryByZoneButton_Click);
      this.urlTextBox.Location = new Point(433, 63);
      this.urlTextBox.Margin = new Padding(4);
      this.urlTextBox.Name = "urlTextBox";
      this.urlTextBox.Size = new Size(471, 22);
      this.urlTextBox.TabIndex = 40;
      this.queryByVehicleButton.Location = new Point(333, 605);
      this.queryByVehicleButton.Margin = new Padding(4);
      this.queryByVehicleButton.Name = "queryByVehicleButton";
      this.queryByVehicleButton.Size = new Size(271, 28);
      this.queryByVehicleButton.TabIndex = 41;
      this.queryByVehicleButton.Text = "Query Parking Rights by Vehicle";
      this.queryByVehicleButton.UseVisualStyleBackColor = true;
      this.queryByVehicleButton.Click += new EventHandler(this.queryByVehicleButton_Click);
      this.vehTextBox.Location = new Point(709, 29);
      this.vehTextBox.Margin = new Padding(4);
      this.vehTextBox.Name = "vehTextBox";
      this.vehTextBox.Size = new Size(143, 22);
      this.vehTextBox.TabIndex = 43;
      this.label5.AutoSize = true;
      this.label5.Location = new Point(618, 33);
      this.label5.Margin = new Padding(4, 0, 4, 0);
      this.label5.Name = "label5";
      this.label5.Size = new Size(58, 17);
      this.label5.TabIndex = 42;
      this.label5.Text = "Vehicle:";
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(1180, 640);
      this.Controls.Add((Control) this.vehTextBox);
      this.Controls.Add((Control) this.label5);
      this.Controls.Add((Control) this.queryByVehicleButton);
      this.Controls.Add((Control) this.urlTextBox);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.helpButton);
      this.Controls.Add((Control) this.passwordTextBox);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.zoneCodeTextBox);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.userIdTextBox);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.listBox1);
      this.Controls.Add((Control) this.queryByZoneButton);
      this.Name = nameof (Form1);
      this.Text = "MobileNow Sandbox";
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Form1()
    {
      this.InitializeComponent();
      this.urlTextBox.Text = "https://us.nowinnovations.eu";
      this.userIdTextBox.Text = "t2inspector";
      this.passwordTextBox.Text = "Test1234";
      this.zoneCodeTextBox.Text = "Z1";
      this.vehTextBox.Text = "CAR1";
    }

    private void queryByZoneButton_Click(object sender, EventArgs e)
    {
      string theUri = this.urlTextBox.Text + "/MobileHTTP.php";
      string parameters = "Login=" + this.userIdTextBox.Text + "/" + this.passwordTextBox.Text + "&DeviceID=12345&Op=zone_check&Data=" + this.zoneCodeTextBox.Text + ",500";
      this.listBox1.Items.Add((object) "Query by Zone:");
      this.listBox1.Items.Add((object) theUri);
      this.listBox1.Items.Add((object) parameters);
      string str1 = this.httpPost(theUri, parameters, -1, "");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "Responce:");
      string str2 = str1.Replace("\n", "\r\n");
      string path = Environment.CurrentDirectory + "\\MobileNow_Tmp.txt";
      StreamWriter streamWriter = new StreamWriter(path);
      streamWriter.WriteLine(str2);
      streamWriter.Close();
      StreamReader streamReader = new StreamReader(path);
      bool flag = true;
      while (true)
      {
        string str3;
        do
        {
          str3 = streamReader.ReadLine();
          if (str3 == null)
            goto label_11;
        }
        while (str3.Trim().Length == 0);
        this.listBox1.Items.Add((object) ("Raw data: " + str3));
        string[] strArray1 = str3.Split(':');
        if (flag)
        {
          if (strArray1.Length >= 4)
            this.listBox1.Items.Add((object) ("Responce code:" + strArray1[0] + "  Zone type:" + strArray1[1] + "  Timestamp:" + this.convertUnixDate(strArray1[2]) + "  Grace period:" + strArray1[3]));
          flag = false;
        }
        else
        {
          string[] strArray2 = str3.Split(':');
          if (strArray2.Length >= 5)
            this.listBox1.Items.Add((object) ("Vehicle:" + strArray2[1] + "  Status:" + this.convertMobileNowStatus(strArray2[2]) + "  Start:" + this.convertUnixDate(strArray2[3]) + "  End:" + this.convertUnixDate(strArray2[4])));
        }
      }
label_11:
      streamReader.Close();
      this.listBox1.Items.Add((object) "");
    }

    private void queryByVehicleButton_Click(object sender, EventArgs e)
    {
      string theUri = this.urlTextBox.Text + "/MobileHTTP.php";
      string parameters = "Login=" + this.userIdTextBox.Text + "/" + this.passwordTextBox.Text + "&DeviceID=12345&Op=check&Data=" + this.vehTextBox.Text;
      this.listBox1.Items.Add((object) "Query by Vehicle:");
      this.listBox1.Items.Add((object) theUri);
      this.listBox1.Items.Add((object) parameters);
      string str = this.httpPost(theUri, parameters, -1, "");
      char[] chArray = new char[1]{ '\n' };
      foreach (object obj in str.Split(chArray))
        this.listBox1.Items.Add(obj);
      this.listBox1.Items.Add((object) "");
    }

    private string convertUnixDate(string inputDate)
    {
      DateTime time = new DateTime(1970, 1, 1, 0, 0, 0);
      try
      {
        int int32 = Convert.ToInt32(inputDate);
        time = time.AddSeconds((double) int32);
        return (time + TimeZone.CurrentTimeZone.GetUtcOffset(time)).ToString("o");
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("Cannot convert date - " + ex.Message));
        return "Invalid";
      }
    }

    private string convertMobileNowStatus(string status)
    {
      if (status == "0")
        return "Not Parking";
      if (status == "1")
        return "Parking";
      if (status == "2")
        return "Alert";
      if (status == "3")
        return "Permit";
      if (status == "4")
        return "Indeterminate Parking";
      return status == "5" ? "Indeterminate Permit" : status;
    }

    private void helpButton_Click(object sender, EventArgs e)
    {
    }

    private string httpPost(string theUri, string parameters, int timeOut, string soapAction)
    {
      try
      {
        ServicePointManager.CertificatePolicy = (ICertificatePolicy) new Form1.TrustAllCertificatePolicy();
        WebRequest webRequest = WebRequest.Create(theUri);
        webRequest.ContentType = "application/x-www-form-urlencoded";
        webRequest.Method = "POST";
        if (soapAction.Length > 0)
          webRequest.Headers.Add("SOAPAction", soapAction);
        if (timeOut > 0)
          webRequest.Timeout = timeOut;
        byte[] bytes = Encoding.ASCII.GetBytes(parameters);
        webRequest.ContentLength = (long) bytes.Length;
        Stream requestStream = webRequest.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();
        WebResponse response;
        try
        {
          response = webRequest.GetResponse();
        }
        catch (WebException ex)
        {
          if (ex.Response != null)
          {
            StreamReader streamReader = new StreamReader(ex.Response.GetResponseStream());
            this.logException("httpPost() - " + streamReader.ReadToEnd(), (Exception) ex);
            streamReader.Close();
          }
          throw;
        }
        if (response == null)
          return "";
        StreamReader streamReader1 = new StreamReader(response.GetResponseStream());
        string str = streamReader1.ReadToEnd().Trim();
        streamReader1.Close();
        return str.Replace("&lt;", "<").Replace("&gt;", ">");
      }
      catch (Exception ex)
      {
        this.logException("httpPost() ERROR", ex);
        return "ERROR";
      }
    }

    private void logException(string theMessage, Exception ex)
    {
      this.listBox1.Items.Add((object) theMessage);
      if (ex == null)
        return;
      this.listBox1.Items.Add((object) ex.Message);
      if (ex.InnerException != null)
        this.listBox1.Items.Add((object) ("Inner Exception: " + ex.InnerException.Message));
      this.listBox1.Items.Add((object) ("Stack Trace: " + ex.StackTrace));
    }

    public class TrustAllCertificatePolicy : ICertificatePolicy
    {
      public bool CheckValidationResult(
        ServicePoint sp,
        X509Certificate cert,
        WebRequest req,
        int problem)
      {
        return true;
      }
    }
  }
}
