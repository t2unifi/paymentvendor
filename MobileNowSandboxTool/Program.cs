﻿// Decompiled with JetBrains decompiler
// Type: MobileNowSandboxTool.Program
// Assembly: MobileNowSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EB0F4647-E969-4CC7-A7D8-1F9B8A31E47D
// Assembly location: C:\SWAN\MEA\MobileNow Sandbox Tool 2012 07 19B\MobileNow Sandbox Tool\MobileNowSandboxTool.exe

using System;
using System.Windows.Forms;

namespace MobileNowSandboxTool
{
  internal static class Program
  {
    [STAThread]
    private static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run((Form) new Form1());
    }
  }
}
