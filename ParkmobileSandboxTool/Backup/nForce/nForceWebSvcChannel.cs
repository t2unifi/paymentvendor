﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.nForce.nForceWebSvcChannel
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace ParkmobileSandboxTool.nForce
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  public interface nForceWebSvcChannel : nForceWebSvc, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
  {
  }
}
