﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.nForce.SupplierZoneInfo
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace ParkmobileSandboxTool.nForce
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "3.0.0.0")]
  [DataContract(Name = "SupplierZoneInfo", Namespace = "http://parkmobile.us/nForce")]
  [Serializable]
  public class SupplierZoneInfo : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private int SupplierIdField;
    [OptionalField]
    private string ZoneCodeField;
    [OptionalField]
    private string ZoneDescriptionField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public int SupplierId
    {
      get
      {
        return this.SupplierIdField;
      }
      set
      {
        if (this.SupplierIdField.Equals(value))
          return;
        this.SupplierIdField = value;
        this.RaisePropertyChanged(nameof (SupplierId));
      }
    }

    [DataMember]
    public string ZoneCode
    {
      get
      {
        return this.ZoneCodeField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.ZoneCodeField, (object) value))
          return;
        this.ZoneCodeField = value;
        this.RaisePropertyChanged(nameof (ZoneCode));
      }
    }

    [DataMember]
    public string ZoneDescription
    {
      get
      {
        return this.ZoneDescriptionField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.ZoneDescriptionField, (object) value))
          return;
        this.ZoneDescriptionField = value;
        this.RaisePropertyChanged(nameof (ZoneDescription));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
