﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.nForce.nForceWebSvc
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;

namespace ParkmobileSandboxTool.nForce
{
  [ServiceContract(ConfigurationName = "nForce.nForceWebSvc", Name = "nForce.Web.Svc", Namespace = "http://parkmobile.us/nForce")]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  public interface nForceWebSvc
  {
    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneObject", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneObjectResponse")]
    ParkingRightDataObject GetParkingRightsByZoneObject(
      string securityToken,
      int supplierId,
      int zoneCode,
      DateTime lastUpdate);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneRangeObject", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneRangeObjectResponse")]
    ParkingRightDataObject GetParkingRightsByZoneRangeObject(
      string securityToken,
      int supplierId,
      int zoneCodeFrom,
      int zoneCodeTo,
      DateTime lastUpdate);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByParkingRightId", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByParkingRightIdResponse")]
    ParkingRightData GetParkingRightsByParkingRightId(int parkingRightId);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByVehicle", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByVehicleResponse")]
    ParkingRightData[] GetParkingRightsByVehicle(int supplierId, string lpn);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneIncremental", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneIncrementalResponse")]
    ParkingRightData[] GetParkingRightsByZoneIncremental(
      int supplierId,
      int zoneCode,
      DateTime lastUpdate);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZone", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneResponse")]
    ParkingRightData[] GetParkingRightsByZone(int supplierId, int zoneCode);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneRange", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneRangeResponse")]
    ParkingRightData[] GetParkingRightsByZoneRange(
      int supplierId,
      int zoneCodeFrom,
      int zoneCodeTo);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneRangeIncremental", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneRangeIncrementalResponse")]
    ParkingRightData[] GetParkingRightsByZoneRangeIncremental(
      int supplierId,
      int zoneCodeFrom,
      int zoneCodeTo,
      DateTime lastUpdate);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneSpaceNumber", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightsByZoneSpaceNumberResponse")]
    ParkingRightData[] GetParkingRightsByZoneSpaceNumber(
      int supplierId,
      int zoneCode,
      string spaceNumber);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightExceptions", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetParkingRightExceptionsResponse")]
    ParkingRightData[] GetParkingRightExceptions(int supplierId);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/GetZonesBySupplierId", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/GetZonesBySupplierIdResponse")]
    SupplierZoneInfo[] GetZonesBySupplierId(int supplierId);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/AuthenticateUser", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/AuthenticateUserResponse")]
    AuthenticatedUserData AuthenticateUser(string userName, string password);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/InsertParkingRights", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/InsertParkingRightsResponse")]
    string InsertParkingRights(
      string securityToken,
      int supplierId,
      int zoneId,
      string spaceNumber,
      DateTime startDate,
      DateTime endDate,
      DateTime maxEndDate,
      string LPN,
      string LPNState,
      string permitId);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/UpdateParkingRights", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/UpdateParkingRightsResponse")]
    string UpdateParkingRights(
      string securityToken,
      int parkingRightId,
      int supplierId,
      int zoneId,
      string spaceNumber,
      DateTime startDate,
      DateTime endDate,
      DateTime maxEndDate);

    [OperationContract(Action = "http://parkmobile.us/nForce/nForce.Web.Svc/DeleteParkingRights", ReplyAction = "http://parkmobile.us/nForce/nForce.Web.Svc/DeleteParkingRightsResponse")]
    string DeleteParkingRights(string securityToken, int parkingRightId);
  }
}
