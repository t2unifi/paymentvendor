﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.nForce.ParkingRightDataObject
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace ParkmobileSandboxTool.nForce
{
  [DataContract(Name = "ParkingRightDataObject", Namespace = "http://parkmobile.us/nForce")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "3.0.0.0")]
  [Serializable]
  public class ParkingRightDataObject : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string ErrorMessageField;
    [OptionalField]
    private byte[] ParkingRightsByZoneField;
    [OptionalField]
    private string ResultTimeStampField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string ErrorMessage
    {
      get
      {
        return this.ErrorMessageField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.ErrorMessageField, (object) value))
          return;
        this.ErrorMessageField = value;
        this.RaisePropertyChanged(nameof (ErrorMessage));
      }
    }

    [DataMember]
    public byte[] ParkingRightsByZone
    {
      get
      {
        return this.ParkingRightsByZoneField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.ParkingRightsByZoneField, (object) value))
          return;
        this.ParkingRightsByZoneField = value;
        this.RaisePropertyChanged(nameof (ParkingRightsByZone));
      }
    }

    [DataMember]
    public string ResultTimeStamp
    {
      get
      {
        return this.ResultTimeStampField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.ResultTimeStampField, (object) value))
          return;
        this.ResultTimeStampField = value;
        this.RaisePropertyChanged(nameof (ResultTimeStamp));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
