﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.Properties.Resources
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace ParkmobileSandboxTool.Properties
{
  [CompilerGenerated]
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (ParkmobileSandboxTool.Properties.Resources.resourceMan == null)
          ParkmobileSandboxTool.Properties.Resources.resourceMan = new ResourceManager("ParkmobileSandboxTool.Properties.Resources", typeof (ParkmobileSandboxTool.Properties.Resources).Assembly);
        return ParkmobileSandboxTool.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return ParkmobileSandboxTool.Properties.Resources.resourceCulture;
      }
      set
      {
        ParkmobileSandboxTool.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
