﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.Form1
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using ParkmobileSandboxTool.nForce;
using System;
using System.ComponentModel;
using System.Drawing;
using System.ServiceModel;
using System.Windows.Forms;

namespace ParkmobileSandboxTool
{
  public class Form1 : Form
  {
    private int theZoneCode = 0;
    private int theSupplierID = 0;
    private string securityToken = "C68367D9-AED3-43D5-A944-13A5CE7509B4";
    private IContainer components = (IContainer) null;
    private Button button1;
    private ListBox listBox1;
    private Label label1;
    private TextBox supplierIdTextBox;
    private TextBox zoneCodeTextBox;
    private Label label2;
    private ComboBox comboBox1;
    private Label label3;
    private TextBox spaceTextBox;
    private Label label4;
    private TextBox sTimeTextBox;
    private Label label6;
    private TextBox eTimeTextBox;
    private Label label7;
    private TextBox stateTextBox;
    private Label label9;
    private TextBox licTextBox;
    private Label label10;
    private Button button2;
    private Button button3;
    private Label label5;
    private Button button4;
    private Label label8;
    private TextBox UrlTextBox;

    public Form1()
    {
      this.InitializeComponent();
      this.supplierIdTextBox.Text = "107010";
      this.zoneCodeTextBox.Text = "2001";
      this.spaceTextBox.Text = "Space1";
      DateTime now = DateTime.Now;
      this.sTimeTextBox.Text = now.ToString("o").Substring(0, 19);
      this.eTimeTextBox.Text = now.AddMinutes(60.0).ToString("o").Substring(0, 19);
      this.licTextBox.Text = "ABC123";
      this.stateTextBox.Text = "IN";
    }

    private bool getSupplierAndZone()
    {
      try
      {
        this.theSupplierID = Convert.ToInt32(this.supplierIdTextBox.Text);
      }
      catch
      {
        int num = (int) MessageBox.Show("Invalid Supplier ID");
        return false;
      }
      try
      {
        this.theZoneCode = Convert.ToInt32(this.zoneCodeTextBox.Text);
      }
      catch
      {
        int num = (int) MessageBox.Show("Invalid Zone Code");
        return false;
      }
      return true;
    }

    private int xlateZoneCode()
    {
      if (this.theZoneCode == 7302000)
        return 37438;
      if (this.theZoneCode == 7302001)
        return 36738;
      if (this.theZoneCode == 7302002)
        return 37443;
      if (this.theZoneCode == 7302004)
        return 37437;
      if (this.theZoneCode == 7302007)
        return 37440;
      if (this.theZoneCode == 7302009)
        return 37442;
      if (this.theZoneCode == 2001)
        return 42899;
      int num = (int) MessageBox.Show("Zone code " + this.theZoneCode.ToString() + " cannot be used to insert parking rights.  It does not belong to the sandbox.");
      return 0;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (!this.getSupplierAndZone())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Please wait...");
      this.listBox1.Refresh();
      ParkingRightData[] parkingRightDataArray;
      try
      {
        nForceWebSvcClient forceWebSvcClient = new nForceWebSvcClient();
        EndpointAddress endpointAddress = new EndpointAddress(this.UrlTextBox.Text);
        forceWebSvcClient.Endpoint.Address = endpointAddress;
        parkingRightDataArray = forceWebSvcClient.GetParkingRightsByZone(Convert.ToInt32(this.theSupplierID), Convert.ToInt32(this.theZoneCode));
      }
      catch (Exception ex)
      {
        this.exceptionHandler(ex);
        parkingRightDataArray = (ParkingRightData[]) null;
      }
      if (parkingRightDataArray != null)
      {
        this.listBox1.Items.Add((object) "Parking Rights:");
        for (int index = 0; index < parkingRightDataArray.Length; ++index)
        {
          ListBox.ObjectCollection items = this.listBox1.Items;
          string[] strArray1 = new string[15];
          strArray1[0] = "Start:'";
          string[] strArray2 = strArray1;
          DateTime dateTime = parkingRightDataArray[index].StartDate.Value;
          string str1 = dateTime.ToString("o");
          strArray2[1] = str1;
          strArray1[2] = "', End:'";
          string[] strArray3 = strArray1;
          dateTime = parkingRightDataArray[index].EndDate.Value;
          string str2 = dateTime.ToString("o");
          strArray3[3] = str2;
          strArray1[4] = "', Space #:'";
          strArray1[5] = parkingRightDataArray[index].SpaceNumber.ToString();
          strArray1[6] = "', License:'";
          strArray1[7] = parkingRightDataArray[index].LPN;
          strArray1[8] = " ";
          strArray1[9] = parkingRightDataArray[index].LPNState;
          strArray1[10] = "', Zone Code:'";
          strArray1[11] = parkingRightDataArray[index].ZoneCode.ToString();
          strArray1[12] = "', Zone ID:'";
          strArray1[13] = parkingRightDataArray[index].ZoneID.ToString();
          strArray1[14] = "'";
          string str3 = string.Concat(strArray1);
          items.Add((object) str3);
        }
      }
      else
        this.listBox1.Items.Add((object) ("No parking rights found for suppler " + this.theSupplierID.ToString() + " and zone code " + this.theZoneCode.ToString() + "."));
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    private void exceptionHandler(Exception ex)
    {
      this.listBox1.Items.Add((object) ("Exception: " + ex.Message));
      if (ex.InnerException == null)
        return;
      this.listBox1.Items.Add((object) ("Exception: " + ex.InnerException.Message));
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      int selectedIndex = this.comboBox1.SelectedIndex;
      if (selectedIndex < 0)
        return;
      this.zoneCodeTextBox.Text = this.comboBox1.Items[selectedIndex].ToString();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      if (!this.getSupplierAndZone())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Please wait...");
      this.listBox1.Refresh();
      DateTime startDate = DateTime.Now;
      DateTime dateTime = DateTime.Now;
      try
      {
        startDate = Convert.ToDateTime(this.sTimeTextBox.Text.Trim());
      }
      catch
      {
        int num = (int) MessageBox.Show("Invalid start time.");
      }
      try
      {
        dateTime = Convert.ToDateTime(this.eTimeTextBox.Text.Trim());
      }
      catch
      {
        int num = (int) MessageBox.Show("Invalid end time.");
      }
      this.listBox1.Items.Add((object) ("Zone Code:" + (object) this.theZoneCode + ", Zone UID:" + this.xlateZoneCode().ToString()));
      this.listBox1.Items.Add((object) ("Data:" + this.spaceTextBox.Text.Trim() + "," + startDate.ToString("o") + "," + dateTime.ToString("o") + "," + this.licTextBox.Text.Trim() + "," + this.stateTextBox.Text.Trim()));
      try
      {
        nForceWebSvcClient forceWebSvcClient = new nForceWebSvcClient();
        EndpointAddress endpointAddress = new EndpointAddress(this.UrlTextBox.Text);
        forceWebSvcClient.Endpoint.Address = endpointAddress;
        var result = forceWebSvcClient.InsertParkingRights(this.securityToken, this.theSupplierID, this.xlateZoneCode(), this.spaceTextBox.Text.Trim(), startDate, dateTime, dateTime, this.licTextBox.Text.Trim(), this.stateTextBox.Text.Trim(), "");
        this.listBox1.Items.Add(result);
      }
      catch (Exception ex)
      {
        this.exceptionHandler(ex);
      }
    }

    private void button3_Click(object sender, EventArgs e)
    {
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "The Supplier ID is the Parkmobile customer ID. This program can be used to insert parking rights into the sandbox.");
      this.listBox1.Items.Add((object) "In this case, the Supplier ID must be 730200 (i.e. customer = 730200 = sandbox). In addition, the sandbox only supports certain zone codes for insertion.");
      this.listBox1.Items.Add((object) "Select one of these zone codes from the ComboBox in the upper right.");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "This program can also be used to query parking rights. In this case, any Supplier ID can be used, so you can use");
      this.listBox1.Items.Add((object) "this program to query parking rights for a customer outside of the sandbox.");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "To query by zone, enter a Supplier ID and a Zone Code, then press the Query by zone button.");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "To query by LPN, enter a Supplier ID and a License, then press the Query by vehicle button.");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "To insert a parking right into the sandbox, select a zone code from the ComboBox, enter Space #, Start and End Dates/Times and License info (optional),");
      this.listBox1.Items.Add((object) "then press the Insert button.");
    }

    private void button4_Click(object sender, EventArgs e)
    {
      if (!this.getSupplierAndZone())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Please wait...");
      this.listBox1.Refresh();
      ParkingRightData[] parkingRightDataArray;
      try
      {
        nForceWebSvcClient forceWebSvcClient = new nForceWebSvcClient();
        EndpointAddress endpointAddress = new EndpointAddress(this.UrlTextBox.Text);
        forceWebSvcClient.Endpoint.Address = endpointAddress;
        parkingRightDataArray = forceWebSvcClient.GetParkingRightsByVehicle(Convert.ToInt32(this.theSupplierID), this.licTextBox.Text.Trim());
      }
      catch (Exception ex)
      {
        this.exceptionHandler(ex);
        parkingRightDataArray = (ParkingRightData[]) null;
      }
      if (parkingRightDataArray != null)
      {
        this.listBox1.Items.Add((object) "Parking Rights:");
        for (int index = 0; index < parkingRightDataArray.Length; ++index)
        {
          ListBox.ObjectCollection items = this.listBox1.Items;
          string[] strArray1 = new string[15];
          strArray1[0] = "Start:'";
          string[] strArray2 = strArray1;
          DateTime dateTime = parkingRightDataArray[index].StartDate.Value;
          string str1 = dateTime.ToString("o");
          strArray2[1] = str1;
          strArray1[2] = "', End:'";
          string[] strArray3 = strArray1;
          dateTime = parkingRightDataArray[index].EndDate.Value;
          string str2 = dateTime.ToString("o");
          strArray3[3] = str2;
          strArray1[4] = "', Space #:'";
          strArray1[5] = parkingRightDataArray[index].SpaceNumber.ToString();
          strArray1[6] = "', License:'";
          strArray1[7] = parkingRightDataArray[index].LPN;
          strArray1[8] = " ";
          strArray1[9] = parkingRightDataArray[index].LPNState;
          strArray1[10] = "', Zone Code:'";
          string[] strArray4 = strArray1;
          int num = parkingRightDataArray[index].ZoneCode;
          string str3 = num.ToString();
          strArray4[11] = str3;
          strArray1[12] = "', Zone ID:'";
          string[] strArray5 = strArray1;
          num = parkingRightDataArray[index].ZoneID;
          string str4 = num.ToString();
          strArray5[13] = str4;
          strArray1[14] = "'";
          string str5 = string.Concat(strArray1);
          items.Add((object) str5);
        }
      }
      else
        this.listBox1.Items.Add((object) ("No parking rights found for suppler " + this.theSupplierID.ToString() + " and vehicle " + this.licTextBox.Text.Trim() + "."));
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.button1 = new Button();
      this.listBox1 = new ListBox();
      this.label1 = new Label();
      this.supplierIdTextBox = new TextBox();
      this.zoneCodeTextBox = new TextBox();
      this.label2 = new Label();
      this.comboBox1 = new ComboBox();
      this.label3 = new Label();
      this.spaceTextBox = new TextBox();
      this.label4 = new Label();
      this.sTimeTextBox = new TextBox();
      this.label6 = new Label();
      this.eTimeTextBox = new TextBox();
      this.label7 = new Label();
      this.stateTextBox = new TextBox();
      this.label9 = new Label();
      this.licTextBox = new TextBox();
      this.label10 = new Label();
      this.button2 = new Button();
      this.button3 = new Button();
      this.label5 = new Label();
      this.button4 = new Button();
      this.label8 = new Label();
      this.UrlTextBox = new TextBox();
      this.SuspendLayout();
      this.button1.Location = new Point(16, 512);
      this.button1.Margin = new Padding(4);
      this.button1.Name = "button1";
      this.button1.Size = new Size(271, 28);
      this.button1.TabIndex = 0;
      this.button1.Text = "Query Parking Rights by Zone";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.listBox1.FormattingEnabled = true;
      this.listBox1.ItemHeight = 16;
      this.listBox1.Location = new Point(16, 149);
      this.listBox1.Margin = new Padding(4);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(1189, 340);
      this.listBox1.TabIndex = 1;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 11);
      this.label1.Margin = new Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(81, 17);
      this.label1.TabIndex = 2;
      this.label1.Text = "Supplier ID:";
      this.supplierIdTextBox.Location = new Point(103, 7);
      this.supplierIdTextBox.Margin = new Padding(4);
      this.supplierIdTextBox.Name = "supplierIdTextBox";
      this.supplierIdTextBox.Size = new Size(143, 22);
      this.supplierIdTextBox.TabIndex = 3;
      this.zoneCodeTextBox.Location = new Point(357, 7);
      this.zoneCodeTextBox.Margin = new Padding(4);
      this.zoneCodeTextBox.Name = "zoneCodeTextBox";
      this.zoneCodeTextBox.Size = new Size(145, 22);
      this.zoneCodeTextBox.TabIndex = 5;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(265, 11);
      this.label2.Margin = new Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new Size(82, 17);
      this.label2.TabIndex = 4;
      this.label2.Text = "Zone Code:";
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Items.AddRange(new object[7]
      {
        (object) "2001",
        (object) "7302000",
        (object) "7302001",
        (object) "7302002",
        (object) "7302004",
        (object) "7302007",
        (object) "7302009"
      });
      this.comboBox1.Location = new Point(863, 7);
      this.comboBox1.Margin = new Padding(4);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(160, 24);
      this.comboBox1.TabIndex = 6;
      this.comboBox1.SelectedIndexChanged += new EventHandler(this.comboBox1_SelectedIndexChanged);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(552, 11);
      this.label3.Margin = new Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new Size(298, 17);
      this.label3.TabIndex = 7;
      this.label3.Text = "To insert rights, use one of these zone codes:";
      this.spaceTextBox.Location = new Point(103, 68);
      this.spaceTextBox.Margin = new Padding(4);
      this.spaceTextBox.Name = "spaceTextBox";
      this.spaceTextBox.Size = new Size((int) sbyte.MaxValue, 22);
      this.spaceTextBox.TabIndex = 9;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(12, 71);
      this.label4.Margin = new Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new Size(64, 17);
      this.label4.TabIndex = 8;
      this.label4.Text = "Space #:";
      this.sTimeTextBox.Location = new Point(357, 68);
      this.sTimeTextBox.Margin = new Padding(4);
      this.sTimeTextBox.Name = "sTimeTextBox";
      this.sTimeTextBox.Size = new Size(284, 22);
      this.sTimeTextBox.TabIndex = 13;
      this.label6.AutoSize = true;
      this.label6.Location = new Point(239, 71);
      this.label6.Margin = new Padding(4, 0, 4, 0);
      this.label6.Name = "label6";
      this.label6.Size = new Size(111, 17);
      this.label6.TabIndex = 12;
      this.label6.Text = "Start Date/Time:";
      this.eTimeTextBox.Location = new Point(357, 117);
      this.eTimeTextBox.Margin = new Padding(4);
      this.eTimeTextBox.Name = "eTimeTextBox";
      this.eTimeTextBox.Size = new Size(284, 22);
      this.eTimeTextBox.TabIndex = 17;
      this.label7.AutoSize = true;
      this.label7.Location = new Point(239, 121);
      this.label7.Margin = new Padding(4, 0, 4, 0);
      this.label7.Name = "label7";
      this.label7.Size = new Size(106, 17);
      this.label7.TabIndex = 16;
      this.label7.Text = "End Date/Time:";
      this.stateTextBox.Location = new Point(856, 108);
      this.stateTextBox.Margin = new Padding(4);
      this.stateTextBox.Name = "stateTextBox";
      this.stateTextBox.Size = new Size(67, 22);
      this.stateTextBox.TabIndex = 21;
      this.label9.AutoSize = true;
      this.label9.Location = new Point(785, 112);
      this.label9.Margin = new Padding(4, 0, 4, 0);
      this.label9.Name = "label9";
      this.label9.Size = new Size(45, 17);
      this.label9.TabIndex = 20;
      this.label9.Text = "State:";
      this.licTextBox.Location = new Point(856, 68);
      this.licTextBox.Margin = new Padding(4);
      this.licTextBox.Name = "licTextBox";
      this.licTextBox.Size = new Size(143, 22);
      this.licTextBox.TabIndex = 19;
      this.label10.AutoSize = true;
      this.label10.Location = new Point(785, 71);
      this.label10.Margin = new Padding(4, 0, 4, 0);
      this.label10.Name = "label10";
      this.label10.Size = new Size(61, 17);
      this.label10.TabIndex = 18;
      this.label10.Text = "License:";
      this.button2.Location = new Point(805, 512);
      this.button2.Margin = new Padding(4);
      this.button2.Name = "button2";
      this.button2.Size = new Size(219, 28);
      this.button2.TabIndex = 22;
      this.button2.Text = "Insert Parking Rights";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.button3.Location = new Point(1095, 512);
      this.button3.Margin = new Padding(4);
      this.button3.Name = "button3";
      this.button3.Size = new Size(112, 28);
      this.button3.TabIndex = 23;
      this.button3.Text = "Help";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.label5.AutoSize = true;
      this.label5.Location = new Point(360, 97);
      this.label5.Margin = new Padding(4, 0, 4, 0);
      this.label5.Name = "label5";
      this.label5.Size = new Size(173, 17);
      this.label5.TabIndex = 24;
      this.label5.Text = "YYYY-MM-DDTHH:MM:SS";
      this.button4.Location = new Point(332, 512);
      this.button4.Margin = new Padding(4);
      this.button4.Name = "button4";
      this.button4.Size = new Size(271, 28);
      this.button4.TabIndex = 25;
      this.button4.Text = "Query Parking Rights by Vehicle";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new EventHandler(this.button4_Click);
      this.label8.AutoSize = true;
      this.label8.Location = new Point(12, 41);
      this.label8.Margin = new Padding(4, 0, 4, 0);
      this.label8.Name = "label8";
      this.label8.Size = new Size(40, 17);
      this.label8.TabIndex = 26;
      this.label8.Text = "URL:";
      this.UrlTextBox.Location = new Point(103, 38);
      this.UrlTextBox.Margin = new Padding(4);
      this.UrlTextBox.Name = "UrlTextBox";
      this.UrlTextBox.Size = new Size(616, 22);
      this.UrlTextBox.TabIndex = 27;
      this.UrlTextBox.Text = "http://staging01.parkmobile.us/nForceWCFService/nForce.svc";
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(1223, 554);
      this.Controls.Add((Control) this.UrlTextBox);
      this.Controls.Add((Control) this.label8);
      this.Controls.Add((Control) this.button4);
      this.Controls.Add((Control) this.label5);
      this.Controls.Add((Control) this.button3);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.stateTextBox);
      this.Controls.Add((Control) this.label9);
      this.Controls.Add((Control) this.licTextBox);
      this.Controls.Add((Control) this.label10);
      this.Controls.Add((Control) this.eTimeTextBox);
      this.Controls.Add((Control) this.label7);
      this.Controls.Add((Control) this.sTimeTextBox);
      this.Controls.Add((Control) this.label6);
      this.Controls.Add((Control) this.spaceTextBox);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.comboBox1);
      this.Controls.Add((Control) this.zoneCodeTextBox);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.supplierIdTextBox);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.listBox1);
      this.Controls.Add((Control) this.button1);
      this.Margin = new Padding(4);
      this.Name = nameof (Form1);
      this.Text = "Parkmobile Sandbox";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
