﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.nForce.nForceWebSvcClient
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace ParkmobileSandboxTool.nForce
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [DebuggerStepThrough]
  public class nForceWebSvcClient : ClientBase<nForceWebSvc>, nForceWebSvc
  {
    public nForceWebSvcClient()
    {
    }
 
    public nForceWebSvcClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public nForceWebSvcClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public nForceWebSvcClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public nForceWebSvcClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public ParkingRightDataObject GetParkingRightsByZoneObject(
      string securityToken,
      int supplierId,
      int zoneCode,
      DateTime lastUpdate)
    {
      return this.Channel.GetParkingRightsByZoneObject(securityToken, supplierId, zoneCode, lastUpdate);
    }

    public ParkingRightDataObject GetParkingRightsByZoneRangeObject(
      string securityToken,
      int supplierId,
      int zoneCodeFrom,
      int zoneCodeTo,
      DateTime lastUpdate)
    {
      return this.Channel.GetParkingRightsByZoneRangeObject(securityToken, supplierId, zoneCodeFrom, zoneCodeTo, lastUpdate);
    }

    public ParkingRightData GetParkingRightsByParkingRightId(int parkingRightId)
    {
      return this.Channel.GetParkingRightsByParkingRightId(parkingRightId);
    }

    public ParkingRightData[] GetParkingRightsByVehicle(int supplierId, string lpn)
    {
      return this.Channel.GetParkingRightsByVehicle(supplierId, lpn);
    }

    public ParkingRightData[] GetParkingRightsByZoneIncremental(
      int supplierId,
      int zoneCode,
      DateTime lastUpdate)
    {
      return this.Channel.GetParkingRightsByZoneIncremental(supplierId, zoneCode, lastUpdate);
    }

    public ParkingRightData[] GetParkingRightsByZone(int supplierId, int zoneCode)
    {
      return this.Channel.GetParkingRightsByZone(supplierId, zoneCode);
    }

    public ParkingRightData[] GetParkingRightsByZoneRange(
      int supplierId,
      int zoneCodeFrom,
      int zoneCodeTo)
    {
      return this.Channel.GetParkingRightsByZoneRange(supplierId, zoneCodeFrom, zoneCodeTo);
    }

    public ParkingRightData[] GetParkingRightsByZoneRangeIncremental(
      int supplierId,
      int zoneCodeFrom,
      int zoneCodeTo,
      DateTime lastUpdate)
    {
      return this.Channel.GetParkingRightsByZoneRangeIncremental(supplierId, zoneCodeFrom, zoneCodeTo, lastUpdate);
    }

    public ParkingRightData[] GetParkingRightsByZoneSpaceNumber(
      int supplierId,
      int zoneCode,
      string spaceNumber)
    {
      return this.Channel.GetParkingRightsByZoneSpaceNumber(supplierId, zoneCode, spaceNumber);
    }

    public ParkingRightData[] GetParkingRightExceptions(int supplierId)
    {
      return this.Channel.GetParkingRightExceptions(supplierId);
    }

    public SupplierZoneInfo[] GetZonesBySupplierId(int supplierId)
    {
      return this.Channel.GetZonesBySupplierId(supplierId);
    }

    public AuthenticatedUserData AuthenticateUser(
      string userName,
      string password)
    {
      return this.Channel.AuthenticateUser(userName, password);
    }

    public string InsertParkingRights(
      string securityToken,
      int supplierId,
      int zoneId,
      string spaceNumber,
      DateTime startDate,
      DateTime endDate,
      DateTime maxEndDate,
      string LPN,
      string LPNState,
      string permitId)
    {
      return this.Channel.InsertParkingRights(securityToken, supplierId, zoneId, spaceNumber, startDate, endDate, maxEndDate, LPN, LPNState, permitId);
    }

    public string UpdateParkingRights(
      string securityToken,
      int parkingRightId,
      int supplierId,
      int zoneId,
      string spaceNumber,
      DateTime startDate,
      DateTime endDate,
      DateTime maxEndDate)
    {
      return this.Channel.UpdateParkingRights(securityToken, parkingRightId, supplierId, zoneId, spaceNumber, startDate, endDate, maxEndDate);
    }

    public string DeleteParkingRights(string securityToken, int parkingRightId)
    {
      return this.Channel.DeleteParkingRights(securityToken, parkingRightId);
    }
  }
}
