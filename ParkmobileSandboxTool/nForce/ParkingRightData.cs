﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.nForce.ParkingRightData
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace ParkmobileSandboxTool.nForce
{
  [DataContract(Name = "ParkingRightData", Namespace = "http://parkmobile.us/nForce")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "3.0.0.0")]
  [Serializable]
  public class ParkingRightData : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private DateTime? EndDateField;
    [OptionalField]
    private string LPNField;
    [OptionalField]
    private string LPNStateField;
    [OptionalField]
    private DateTime? ModifiedDateField;
    [OptionalField]
    private int ParkingRightIDField;
    [OptionalField]
    private int PayedMinutesField;
    [OptionalField]
    private string PermitField;
    [OptionalField]
    private string ProductDescriptionField;
    [OptionalField]
    private int ProductTypeIdField;
    [OptionalField]
    private Decimal PurchaseAmountField;
    [OptionalField]
    private string SpaceNumberField;
    [OptionalField]
    private DateTime? StartDateField;
    [OptionalField]
    private string TimeZoneField;
    [OptionalField]
    private string VehicleColorField;
    [OptionalField]
    private string VehicleMakeField;
    [OptionalField]
    private string VehicleModelField;
    [OptionalField]
    private int ZoneCodeField;
    [OptionalField]
    private int ZoneIDField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public DateTime? EndDate
    {
      get
      {
        return this.EndDateField;
      }
      set
      {
        if (this.EndDateField.Equals((object) value))
          return;
        this.EndDateField = value;
        this.RaisePropertyChanged(nameof (EndDate));
      }
    }

    [DataMember]
    public string LPN
    {
      get
      {
        return this.LPNField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.LPNField, (object) value))
          return;
        this.LPNField = value;
        this.RaisePropertyChanged(nameof (LPN));
      }
    }

    [DataMember]
    public string LPNState
    {
      get
      {
        return this.LPNStateField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.LPNStateField, (object) value))
          return;
        this.LPNStateField = value;
        this.RaisePropertyChanged(nameof (LPNState));
      }
    }

    [DataMember]
    public DateTime? ModifiedDate
    {
      get
      {
        return this.ModifiedDateField;
      }
      set
      {
        if (this.ModifiedDateField.Equals((object) value))
          return;
        this.ModifiedDateField = value;
        this.RaisePropertyChanged(nameof (ModifiedDate));
      }
    }

    [DataMember]
    public int ParkingRightID
    {
      get
      {
        return this.ParkingRightIDField;
      }
      set
      {
        if (this.ParkingRightIDField.Equals(value))
          return;
        this.ParkingRightIDField = value;
        this.RaisePropertyChanged(nameof (ParkingRightID));
      }
    }

    [DataMember]
    public int PayedMinutes
    {
      get
      {
        return this.PayedMinutesField;
      }
      set
      {
        if (this.PayedMinutesField.Equals(value))
          return;
        this.PayedMinutesField = value;
        this.RaisePropertyChanged(nameof (PayedMinutes));
      }
    }

    [DataMember]
    public string Permit
    {
      get
      {
        return this.PermitField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.PermitField, (object) value))
          return;
        this.PermitField = value;
        this.RaisePropertyChanged(nameof (Permit));
      }
    }

    [DataMember]
    public string ProductDescription
    {
      get
      {
        return this.ProductDescriptionField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.ProductDescriptionField, (object) value))
          return;
        this.ProductDescriptionField = value;
        this.RaisePropertyChanged(nameof (ProductDescription));
      }
    }

    [DataMember]
    public int ProductTypeId
    {
      get
      {
        return this.ProductTypeIdField;
      }
      set
      {
        if (this.ProductTypeIdField.Equals(value))
          return;
        this.ProductTypeIdField = value;
        this.RaisePropertyChanged(nameof (ProductTypeId));
      }
    }

    [DataMember]
    public Decimal PurchaseAmount
    {
      get
      {
        return this.PurchaseAmountField;
      }
      set
      {
        if (this.PurchaseAmountField.Equals(value))
          return;
        this.PurchaseAmountField = value;
        this.RaisePropertyChanged(nameof (PurchaseAmount));
      }
    }

    [DataMember]
    public string SpaceNumber
    {
      get
      {
        return this.SpaceNumberField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.SpaceNumberField, (object) value))
          return;
        this.SpaceNumberField = value;
        this.RaisePropertyChanged(nameof (SpaceNumber));
      }
    }

    [DataMember]
    public DateTime? StartDate
    {
      get
      {
        return this.StartDateField;
      }
      set
      {
        if (this.StartDateField.Equals((object) value))
          return;
        this.StartDateField = value;
        this.RaisePropertyChanged(nameof (StartDate));
      }
    }

    [DataMember]
    public string TimeZone
    {
      get
      {
        return this.TimeZoneField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.TimeZoneField, (object) value))
          return;
        this.TimeZoneField = value;
        this.RaisePropertyChanged(nameof (TimeZone));
      }
    }

    [DataMember]
    public string VehicleColor
    {
      get
      {
        return this.VehicleColorField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.VehicleColorField, (object) value))
          return;
        this.VehicleColorField = value;
        this.RaisePropertyChanged(nameof (VehicleColor));
      }
    }

    [DataMember]
    public string VehicleMake
    {
      get
      {
        return this.VehicleMakeField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.VehicleMakeField, (object) value))
          return;
        this.VehicleMakeField = value;
        this.RaisePropertyChanged(nameof (VehicleMake));
      }
    }

    [DataMember]
    public string VehicleModel
    {
      get
      {
        return this.VehicleModelField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.VehicleModelField, (object) value))
          return;
        this.VehicleModelField = value;
        this.RaisePropertyChanged(nameof (VehicleModel));
      }
    }

    [DataMember]
    public int ZoneCode
    {
      get
      {
        return this.ZoneCodeField;
      }
      set
      {
        if (this.ZoneCodeField.Equals(value))
          return;
        this.ZoneCodeField = value;
        this.RaisePropertyChanged(nameof (ZoneCode));
      }
    }

    [DataMember]
    public int ZoneID
    {
      get
      {
        return this.ZoneIDField;
      }
      set
      {
        if (this.ZoneIDField.Equals(value))
          return;
        this.ZoneIDField = value;
        this.RaisePropertyChanged(nameof (ZoneID));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
