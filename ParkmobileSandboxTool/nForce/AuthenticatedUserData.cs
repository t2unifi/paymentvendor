﻿// Decompiled with JetBrains decompiler
// Type: ParkmobileSandboxTool.nForce.AuthenticatedUserData
// Assembly: ParkmobileSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7F96AB4A-ED27-4124-8D16-5915B8291EA5
// Assembly location: C:\SWAN\MEA\Parkmobile Sandbox Tool 2014 04 21\ParkmobileSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace ParkmobileSandboxTool.nForce
{
  [DataContract(Name = "AuthenticatedUserData", Namespace = "http://parkmobile.us/nForce")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "3.0.0.0")]
  [Serializable]
  public class AuthenticatedUserData : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string EmailField;
    [OptionalField]
    private bool EnabledField;
    [OptionalField]
    private string FirstNameField;
    [OptionalField]
    private bool IsAuthenticatedField;
    [OptionalField]
    private string LastNameField;
    [OptionalField]
    private string PasswordField;
    [OptionalField]
    private int SupplierIdField;
    [OptionalField]
    private int SystemSupplierIdField;
    [OptionalField]
    private string UserNameField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string Email
    {
      get
      {
        return this.EmailField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.EmailField, (object) value))
          return;
        this.EmailField = value;
        this.RaisePropertyChanged(nameof (Email));
      }
    }

    [DataMember]
    public bool Enabled
    {
      get
      {
        return this.EnabledField;
      }
      set
      {
        if (this.EnabledField.Equals(value))
          return;
        this.EnabledField = value;
        this.RaisePropertyChanged(nameof (Enabled));
      }
    }

    [DataMember]
    public string FirstName
    {
      get
      {
        return this.FirstNameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.FirstNameField, (object) value))
          return;
        this.FirstNameField = value;
        this.RaisePropertyChanged(nameof (FirstName));
      }
    }

    [DataMember]
    public bool IsAuthenticated
    {
      get
      {
        return this.IsAuthenticatedField;
      }
      set
      {
        if (this.IsAuthenticatedField.Equals(value))
          return;
        this.IsAuthenticatedField = value;
        this.RaisePropertyChanged(nameof (IsAuthenticated));
      }
    }

    [DataMember]
    public string LastName
    {
      get
      {
        return this.LastNameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.LastNameField, (object) value))
          return;
        this.LastNameField = value;
        this.RaisePropertyChanged(nameof (LastName));
      }
    }

    [DataMember]
    public string Password
    {
      get
      {
        return this.PasswordField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.PasswordField, (object) value))
          return;
        this.PasswordField = value;
        this.RaisePropertyChanged(nameof (Password));
      }
    }

    [DataMember]
    public int SupplierId
    {
      get
      {
        return this.SupplierIdField;
      }
      set
      {
        if (this.SupplierIdField.Equals(value))
          return;
        this.SupplierIdField = value;
        this.RaisePropertyChanged(nameof (SupplierId));
      }
    }

    [DataMember]
    public int SystemSupplierId
    {
      get
      {
        return this.SystemSupplierIdField;
      }
      set
      {
        if (this.SystemSupplierIdField.Equals(value))
          return;
        this.SystemSupplierIdField = value;
        this.RaisePropertyChanged(nameof (SystemSupplierId));
      }
    }

    [DataMember]
    public string UserName
    {
      get
      {
        return this.UserNameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.UserNameField, (object) value))
          return;
        this.UserNameField = value;
        this.RaisePropertyChanged(nameof (UserName));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
