﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.Form1
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace DigitalSandboxTool
{
  public class Form1 : Form
  {
    private IContainer components = (IContainer) null;
    private Button stallsByZoneButton;
    private ListBox listBox1;
    private Label label1;
    private TextBox userIdTextBox;
    private TextBox zoneCodeTextBox;
    private Label label2;
    private TextBox passwordTextBox;
    private Label label4;
    private TextBox stateTextBox;
    private Label label9;
    private TextBox licTextBox;
    private Label label10;
    private Button helpButton;
    private Button vehByZoneButton;
    private TextBox tokenTextBox;
    private Label label3;
    private TextBox urlTextBox;
    private Label label8;
    private Button listZonesButton;
    private Button lpnButton;
    private Label label5;
    private ComboBox comboBox1;
    private Button listZonesStButton;
    private Label label6;
    private Label label7;
    private Label label11;
    private TextBox startTextBox;
    private TextBox endTextBox;
    private TextBox token2TextBox;
    private Label label12;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.stallsByZoneButton = new Button();
      this.listBox1 = new ListBox();
      this.label1 = new Label();
      this.userIdTextBox = new TextBox();
      this.zoneCodeTextBox = new TextBox();
      this.label2 = new Label();
      this.passwordTextBox = new TextBox();
      this.label4 = new Label();
      this.stateTextBox = new TextBox();
      this.label9 = new Label();
      this.licTextBox = new TextBox();
      this.label10 = new Label();
      this.helpButton = new Button();
      this.vehByZoneButton = new Button();
      this.tokenTextBox = new TextBox();
      this.label3 = new Label();
      this.urlTextBox = new TextBox();
      this.label8 = new Label();
      this.listZonesButton = new Button();
      this.lpnButton = new Button();
      this.label5 = new Label();
      this.comboBox1 = new ComboBox();
      this.listZonesStButton = new Button();
      this.label6 = new Label();
      this.label7 = new Label();
      this.label11 = new Label();
      this.startTextBox = new TextBox();
      this.endTextBox = new TextBox();
      this.token2TextBox = new TextBox();
      this.label12 = new Label();
      this.SuspendLayout();
      this.stallsByZoneButton.Location = new Point(189, 901);
      this.stallsByZoneButton.Margin = new Padding(4);
      this.stallsByZoneButton.Name = "stallsByZoneButton";
      this.stallsByZoneButton.Size = new Size(176, 28);
      this.stallsByZoneButton.TabIndex = 0;
      this.stallsByZoneButton.Text = "Query Stalls by Zone";
      this.stallsByZoneButton.UseVisualStyleBackColor = true;
      this.stallsByZoneButton.Click += new EventHandler(this.stallsByZoneButton_Click);
      this.listBox1.FormattingEnabled = true;
      this.listBox1.HorizontalScrollbar = true;
      this.listBox1.ItemHeight = 16;
      this.listBox1.Location = new Point(16, 149);
      this.listBox1.Margin = new Padding(4);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(1189, 724);
      this.listBox1.TabIndex = 1;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 57);
      this.label1.Margin = new Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(59, 17);
      this.label1.TabIndex = 2;
      this.label1.Text = "User ID:";
      this.userIdTextBox.Location = new Point(121, 52);
      this.userIdTextBox.Margin = new Padding(4);
      this.userIdTextBox.Name = "userIdTextBox";
      this.userIdTextBox.Size = new Size(223, 22);
      this.userIdTextBox.TabIndex = 3;
      this.zoneCodeTextBox.Location = new Point(547, 53);
      this.zoneCodeTextBox.Margin = new Padding(4);
      this.zoneCodeTextBox.Name = "zoneCodeTextBox";
      this.zoneCodeTextBox.Size = new Size(145, 22);
      this.zoneCodeTextBox.TabIndex = 5;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(400, 57);
      this.label2.Margin = new Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new Size(141, 17);
      this.label2.TabIndex = 4;
      this.label2.Text = "Zone Code (Region):";
      this.passwordTextBox.Location = new Point(120, 112);
      this.passwordTextBox.Margin = new Padding(4);
      this.passwordTextBox.Name = "passwordTextBox";
      this.passwordTextBox.Size = new Size((int) sbyte.MaxValue, 22);
      this.passwordTextBox.TabIndex = 9;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(12, 117);
      this.label4.Margin = new Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new Size(73, 17);
      this.label4.TabIndex = 8;
      this.label4.Text = "Password:";
      this.stateTextBox.Location = new Point(856, 108);
      this.stateTextBox.Margin = new Padding(4);
      this.stateTextBox.Name = "stateTextBox";
      this.stateTextBox.Size = new Size(67, 22);
      this.stateTextBox.TabIndex = 21;
      this.label9.AutoSize = true;
      this.label9.Location = new Point(785, 112);
      this.label9.Margin = new Padding(4, 0, 4, 0);
      this.label9.Name = "label9";
      this.label9.Size = new Size(45, 17);
      this.label9.TabIndex = 20;
      this.label9.Text = "State:";
      this.licTextBox.Location = new Point(856, 42);
      this.licTextBox.Margin = new Padding(4);
      this.licTextBox.Name = "licTextBox";
      this.licTextBox.Size = new Size(143, 22);
      this.licTextBox.TabIndex = 19;
      this.label10.AutoSize = true;
      this.label10.Location = new Point(785, 45);
      this.label10.Margin = new Padding(4, 0, 4, 0);
      this.label10.Name = "label10";
      this.label10.Size = new Size(61, 17);
      this.label10.TabIndex = 18;
      this.label10.Text = "License:";
      this.helpButton.Location = new Point(1096, 900);
      this.helpButton.Margin = new Padding(4);
      this.helpButton.Name = "helpButton";
      this.helpButton.Size = new Size(112, 28);
      this.helpButton.TabIndex = 23;
      this.helpButton.Text = "Help";
      this.helpButton.UseVisualStyleBackColor = true;
      this.helpButton.Click += new EventHandler(this.helpButton_Click);
      this.vehByZoneButton.Location = new Point(576, 901);
      this.vehByZoneButton.Margin = new Padding(4);
      this.vehByZoneButton.Name = "vehByZoneButton";
      this.vehByZoneButton.Size = new Size(183, 28);
      this.vehByZoneButton.TabIndex = 25;
      this.vehByZoneButton.Text = "Query by Plates by Zone";
      this.vehByZoneButton.UseVisualStyleBackColor = true;
      this.vehByZoneButton.Click += new EventHandler(this.vehByZoneButton_Click);
      this.tokenTextBox.Location = new Point(121, 82);
      this.tokenTextBox.Margin = new Padding(4);
      this.tokenTextBox.Name = "tokenTextBox";
      this.tokenTextBox.Size = new Size(455, 22);
      this.tokenTextBox.TabIndex = 27;
      this.label3.AutoSize = true;
      this.label3.Location = new Point(12, 86);
      this.label3.Margin = new Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new Size(106, 17);
      this.label3.TabIndex = 26;
      this.label3.Text = "StallInfo Token:";
      this.urlTextBox.Location = new Point(619, 11);
      this.urlTextBox.Margin = new Padding(4);
      this.urlTextBox.Name = "urlTextBox";
      this.urlTextBox.Size = new Size(527, 22);
      this.urlTextBox.TabIndex = 29;
      this.label8.AutoSize = true;
      this.label8.Location = new Point(568, 15);
      this.label8.Margin = new Padding(4, 0, 4, 0);
      this.label8.Name = "label8";
      this.label8.Size = new Size(40, 17);
      this.label8.TabIndex = 28;
      this.label8.Text = "URL:";
      this.listZonesButton.Location = new Point(466, 901);
      this.listZonesButton.Margin = new Padding(4);
      this.listZonesButton.Name = "listZonesButton";
      this.listZonesButton.Size = new Size(101, 28);
      this.listZonesButton.TabIndex = 30;
      this.listZonesButton.Text = "List Zones";
      this.listZonesButton.UseVisualStyleBackColor = true;
      this.listZonesButton.Click += new EventHandler(this.listZonesButton_Click);
      this.lpnButton.Location = new Point(766, 901);
      this.lpnButton.Margin = new Padding(4);
      this.lpnButton.Name = "lpnButton";
      this.lpnButton.Size = new Size(159, 28);
      this.lpnButton.TabIndex = 31;
      this.lpnButton.Text = "Query by LPN";
      this.lpnButton.UseVisualStyleBackColor = true;
      this.lpnButton.Click += new EventHandler(this.lpnButton_Click);
      this.label5.AutoSize = true;
      this.label5.Location = new Point(12, 15);
      this.label5.Margin = new Padding(4, 0, 4, 0);
      this.label5.Name = "label5";
      this.label5.Size = new Size(67, 17);
      this.label5.TabIndex = 32;
      this.label5.Text = "Sandbox:";
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Items.AddRange(new object[3]
      {
        (object) "1- Old API Sandbox",
        (object) "2- New API Sandbox",
        (object) "3- Other"
      });
      this.comboBox1.Location = new Point(121, 11);
      this.comboBox1.Margin = new Padding(4);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(303, 24);
      this.comboBox1.TabIndex = 33;
      this.comboBox1.SelectedIndexChanged += new EventHandler(this.comboBox1_SelectedIndexChanged);
      this.listZonesStButton.Location = new Point(17, 901);
      this.listZonesStButton.Margin = new Padding(4);
      this.listZonesStButton.Name = "listZonesStButton";
      this.listZonesStButton.Size = new Size(164, 28);
      this.listZonesStButton.TabIndex = 34;
      this.listZonesStButton.Text = "List Zones";
      this.listZonesStButton.UseVisualStyleBackColor = true;
      this.listZonesStButton.Click += new EventHandler(this.listZonesStButton_Click);
      this.label6.AutoSize = true;
      this.label6.Location = new Point(13, 882);
      this.label6.Margin = new Padding(4, 0, 4, 0);
      this.label6.Name = "label6";
      this.label6.Size = new Size(414, 17);
      this.label6.TabIndex = 35;
      this.label6.Text = "<------------------------- StallInfoService -------------------------------->";
      this.label7.AutoSize = true;
      this.label7.Location = new Point(462, 882);
      this.label7.Margin = new Padding(4, 0, 4, 0);
      this.label7.Name = "label7";
      this.label7.Size = new Size(554, 17);
      this.label7.TabIndex = 36;
      this.label7.Text = "<------------------------------------ PlateInfoService ------------------------------------------------>";
      this.label11.AutoSize = true;
      this.label11.Location = new Point(310, 117);
      this.label11.Margin = new Padding(4, 0, 4, 0);
      this.label11.Name = "label11";
      this.label11.Size = new Size(140, 17);
      this.label11.TabIndex = 37;
      this.label11.Text = "Starting/Ending Stall:";
      this.startTextBox.Location = new Point(458, 112);
      this.startTextBox.Margin = new Padding(4);
      this.startTextBox.Name = "startTextBox";
      this.startTextBox.Size = new Size(67, 22);
      this.startTextBox.TabIndex = 38;
      this.endTextBox.Location = new Point(545, 112);
      this.endTextBox.Margin = new Padding(4);
      this.endTextBox.Name = "endTextBox";
      this.endTextBox.Size = new Size(84, 22);
      this.endTextBox.TabIndex = 39;
      this.token2TextBox.Location = new Point(706, 83);
      this.token2TextBox.Margin = new Padding(4);
      this.token2TextBox.Name = "token2TextBox";
      this.token2TextBox.Size = new Size(455, 22);
      this.token2TextBox.TabIndex = 40;
      this.label12.AutoSize = true;
      this.label12.Location = new Point(588, 85);
      this.label12.Margin = new Padding(4, 0, 4, 0);
      this.label12.Name = "label12";
      this.label12.Size = new Size(111, 17);
      this.label12.TabIndex = 41;
      this.label12.Text = "PlateInfo Token:";
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(1223, 956);
      this.Controls.Add((Control) this.label12);
      this.Controls.Add((Control) this.token2TextBox);
      this.Controls.Add((Control) this.endTextBox);
      this.Controls.Add((Control) this.startTextBox);
      this.Controls.Add((Control) this.label11);
      this.Controls.Add((Control) this.label7);
      this.Controls.Add((Control) this.label6);
      this.Controls.Add((Control) this.listZonesStButton);
      this.Controls.Add((Control) this.comboBox1);
      this.Controls.Add((Control) this.label5);
      this.Controls.Add((Control) this.lpnButton);
      this.Controls.Add((Control) this.listZonesButton);
      this.Controls.Add((Control) this.urlTextBox);
      this.Controls.Add((Control) this.label8);
      this.Controls.Add((Control) this.tokenTextBox);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.vehByZoneButton);
      this.Controls.Add((Control) this.helpButton);
      this.Controls.Add((Control) this.stateTextBox);
      this.Controls.Add((Control) this.label9);
      this.Controls.Add((Control) this.licTextBox);
      this.Controls.Add((Control) this.label10);
      this.Controls.Add((Control) this.passwordTextBox);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.zoneCodeTextBox);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.userIdTextBox);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.listBox1);
      this.Controls.Add((Control) this.stallsByZoneButton);
      this.Margin = new Padding(4);
      this.Name = nameof (Form1);
      this.Text = "Digital Sandbox";
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Form1()
    {
      this.InitializeComponent();
      this.startTextBox.Text = "1";
      this.endTextBox.Text = "99999";
    }

    private void listZonesStButton_Click(object sender, EventArgs e)
    {
      if (!this.checkSandboxSelection())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Please wait...");
      this.listBox1.Refresh();
      try
      {
        string str1 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:stal=\"http://ws.digitalpaytech.com/stallInfo\">" + "\r\n<soapenv:Header>" + "\r\n<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" + "\r\n<wsse:UsernameToken wsu:Id=\"UsernameToken-32925455\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" + "\r\n<wsse:Username>" + this.userIdTextBox.Text + "</wsse:Username>" + "\r\n<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + this.passwordTextBox.Text + "</wsse:Password>" + "\r\n<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">xV5+5+AMWvDoqV+eqLZsYQ==</wsse:Nonce>" + "\r\n<wsu:Created>" + DateTime.Now.ToString("o") + "</wsu:Created>" + "\r\n</wsse:UsernameToken>" + "\r\n</wsse:Security>" + "\r\n</soapenv:Header>" + "\r\n<soapenv:Body>" + "\r\n<stal:RegionRequest>" + "\r\n<token>" + this.tokenTextBox.Text + "</token>" + "\r\n</stal:RegionRequest>" + "\r\n</soapenv:Body>" + "\r\n</soapenv:Envelope>";
        string theUri = this.urlTextBox.Text + "/StallInfoService";
        this.listBox1.Items.Add((object) theUri);
        this.listBox1.Items.Add((object) "");
        this.showXml(str1, '\n');
        this.listBox1.Items.Add((object) "");
        string soapAction = "http://ws.digitalpaytech.com/stallInfo/getRegions";
        string str2 = "2";
        if (this.comboBox1.SelectedItem != null)
          str2 = this.comboBox1.SelectedItem.ToString();
        if (str2.StartsWith("1"))
          soapAction = "http://ws.digitalpioneer.com/stallInfo/getRegions";
        this.listBox1.Items.Add((object) ("SOAPAction: " + soapAction));
        this.listBox1.Items.Add((object) "PLEASE WAIT...");
        this.listBox1.Items.Add((object) "--------------");
        this.listBox1.Items.Add((object) "");
        this.listBox1.Refresh();
        string xml = this.httpPost(theUri, str1, -1, soapAction);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) "REPLY:");
        this.showXml(xml, '<');
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) "Parsed Data:");
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(xml);
        foreach (XmlNode selectNode in xmlDocument.SelectNodes("//regions"))
          this.listBox1.Items.Add((object) (selectNode["regionName"].InnerText + " " + selectNode["description"].InnerText));
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("ERROR " + ex.Message));
        if (ex.InnerException != null)
          this.listBox1.Items.Add((object) ex.InnerException);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) ex.StackTrace);
      }
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    private void stallsByZoneButton_Click(object sender, EventArgs e)
    {
      if (!this.checkSandboxSelection() || !this.checkZoneSelection())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Refresh();
      try
      {
        DateTime now = DateTime.Now;
        string[] strArray1 = new string[11];
        string[] strArray2 = strArray1;
        int num = now.Year;
        string str1 = num.ToString("D4");
        strArray2[0] = str1;
        strArray1[1] = "-";
        string[] strArray3 = strArray1;
        num = now.Month;
        string str2 = num.ToString("D2");
        strArray3[2] = str2;
        strArray1[3] = "-";
        string[] strArray4 = strArray1;
        num = now.Day;
        string str3 = num.ToString("D2");
        strArray4[4] = str3;
        strArray1[5] = "T";
        string[] strArray5 = strArray1;
        num = now.Hour;
        string str4 = num.ToString();
        strArray5[6] = str4;
        strArray1[7] = ":";
        string[] strArray6 = strArray1;
        num = now.Minute;
        string str5 = num.ToString();
        strArray6[8] = str5;
        strArray1[9] = ":";
        string[] strArray7 = strArray1;
        num = now.Second;
        string str6 = num.ToString();
        strArray7[10] = str6;
        string str7 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:stal=\"http://ws.digitalpaytech.com/stallInfo\">" + "\r\n<soapenv:Header>" + "\r\n<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" + "\r\n<wsse:UsernameToken wsu:Id=\"UsernameToken-32925455\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" + "\r\n<wsse:Username>" + this.userIdTextBox.Text + "</wsse:Username>" + "\r\n<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + this.passwordTextBox.Text + "</wsse:Password>" + "\r\n<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">xV5+5+AMWvDoqV+eqLZsYQ==</wsse:Nonce>" + "\r\n<wsu:Created>" + string.Concat(strArray1) + "</wsu:Created>" + "\r\n</wsse:UsernameToken>" + "\r\n</wsse:Security>" + "\r\n</soapenv:Header>" + "\r\n<soapenv:Body>" + "\r\n<stal:StallInfoByRegionRequest>" + "\r\n<token>" + this.tokenTextBox.Text + "</token>" + "\r\n<region>" + this.zoneCodeTextBox.Text + "</region>" + "\r\n<stallfrom>" + this.startTextBox.Text.Trim() + "</stallfrom>" + "\r\n<stallto>" + this.endTextBox.Text.Trim() + "</stallto>" + "\r\n<stallstatus>" + "All" + "</stallstatus>" + "\r\n<datetimeStamp/>" + "\r\n</stal:StallInfoByRegionRequest>" + "\r\n</soapenv:Body>" + "\r\n</soapenv:Envelope>";
        string theUri = this.urlTextBox.Text + "/StallInfoService";
        this.listBox1.Items.Add((object) theUri);
        this.listBox1.Items.Add((object) "");
        this.showXml(str7, '\n');
        this.listBox1.Items.Add((object) "");
        string soapAction = "http://ws.digitalpaytech.com/stallInfo/getStallInfoByRegion";
        this.listBox1.Items.Add((object) ("SOAPAction: " + soapAction));
        this.listBox1.Items.Add((object) "PLEASE WAIT...");
        this.listBox1.Items.Add((object) "--------------");
        this.listBox1.Items.Add((object) "");
        this.listBox1.Refresh();
        string xml = this.httpPost(theUri, str7, -1, soapAction);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) "REPLY:");
        this.showXml(xml, '<');
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("ERROR " + ex.Message));
        if (ex.InnerException != null)
          this.listBox1.Items.Add((object) ex.InnerException);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) ex.StackTrace);
      }
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    private void listZonesButton_Click(object sender, EventArgs e)
    {
      if (!this.checkSandboxSelection())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Please wait...");
      this.listBox1.Refresh();
      try
      {
        string str = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:stal=\"http://ws.digitalpaytech.com/plateInfo\">" + "\r\n<soapenv:Header>" + "\r\n<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" + "\r\n<wsse:UsernameToken wsu:Id=\"UsernameToken-32925455\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" + "\r\n<wsse:Username>" + this.userIdTextBox.Text + "</wsse:Username>" + "\r\n<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + this.passwordTextBox.Text + "</wsse:Password>" + "\r\n<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">xV5+5+AMWvDoqV+eqLZsYQ==</wsse:Nonce>" + "\r\n<wsu:Created>" + DateTime.Now.ToString("o") + "</wsu:Created>" + "\r\n</wsse:UsernameToken>" + "\r\n</wsse:Security>" + "\r\n</soapenv:Header>" + "\r\n<soapenv:Body>" + "\r\n<stal:RegionRequest>" + "\r\n<token>" + this.token2TextBox.Text + "</token>" + "\r\n</stal:RegionRequest>" + "\r\n</soapenv:Body>" + "\r\n</soapenv:Envelope>";
        string theUri = this.urlTextBox.Text + "/PlateInfoService";
        this.listBox1.Items.Add((object) theUri);
        this.listBox1.Items.Add((object) "");
        this.showXml(str, '\n');
        this.listBox1.Items.Add((object) "");
        string soapAction = "http://ws.digitalpaytech.com/plateInfo/getRegions";
        this.listBox1.Items.Add((object) ("SOAPAction: " + soapAction));
        this.listBox1.Items.Add((object) "PLEASE WAIT...");
        this.listBox1.Items.Add((object) "--------------");
        this.listBox1.Items.Add((object) "");
        this.listBox1.Refresh();
        string xml = this.httpPost(theUri, str, -1, soapAction);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) "REPLY:");
        this.showXml(xml, '<');
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) "Parsed Data:");
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(xml);
        foreach (XmlNode selectNode in xmlDocument.SelectNodes("//regions"))
          this.listBox1.Items.Add((object) (selectNode["regionName"].InnerText + " " + selectNode["description"].InnerText));
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("ERROR " + ex.Message));
        if (ex.InnerException != null)
          this.listBox1.Items.Add((object) ex.InnerException);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) ex.StackTrace);
      }
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    private void vehByZoneButton_Click(object sender, EventArgs e)
    {
      if (!this.checkSandboxSelection() || !this.checkZoneSelection())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Refresh();
      try
      {
        DateTime now = DateTime.Now;
        string[] strArray1 = new string[11];
        string[] strArray2 = strArray1;
        int num = now.Year;
        string str1 = num.ToString("D4");
        strArray2[0] = str1;
        strArray1[1] = "-";
        string[] strArray3 = strArray1;
        num = now.Month;
        string str2 = num.ToString("D2");
        strArray3[2] = str2;
        strArray1[3] = "-";
        string[] strArray4 = strArray1;
        num = now.Day;
        string str3 = num.ToString("D2");
        strArray4[4] = str3;
        strArray1[5] = "T";
        string[] strArray5 = strArray1;
        num = now.Hour;
        string str4 = num.ToString();
        strArray5[6] = str4;
        strArray1[7] = ":";
        string[] strArray6 = strArray1;
        num = now.Minute;
        string str5 = num.ToString();
        strArray6[8] = str5;
        strArray1[9] = ":";
        string[] strArray7 = strArray1;
        num = now.Second;
        string str6 = num.ToString();
        strArray7[10] = str6;
        string str7 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:stal=\"http://ws.digitalpaytech.com/plateInfo\">" + "\r\n<soapenv:Header>" + "\r\n<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" + "\r\n<wsse:UsernameToken wsu:Id=\"UsernameToken-32925455\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" + "\r\n<wsse:Username>" + this.userIdTextBox.Text + "</wsse:Username>" + "\r\n<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + this.passwordTextBox.Text + "</wsse:Password>" + "\r\n<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">xV5+5+AMWvDoqV+eqLZsYQ==</wsse:Nonce>" + "\r\n<wsu:Created>" + string.Concat(strArray1) + "</wsu:Created>" + "\r\n</wsse:UsernameToken>" + "\r\n</wsse:Security>" + "\r\n</soapenv:Header>" + "\r\n<soapenv:Body>" + "\r\n<stal:PlateInfoByRegionRequest>" + "\r\n<token>" + this.token2TextBox.Text + "</token>" + "\r\n<region>" + this.zoneCodeTextBox.Text + "</region>" + "\r\n</stal:PlateInfoByRegionRequest>" + "\r\n</soapenv:Body>" + "\r\n</soapenv:Envelope>";
        string theUri = this.urlTextBox.Text + "/PlateInfoService";
        this.listBox1.Items.Add((object) theUri);
        this.listBox1.Items.Add((object) "");
        this.showXml(str7, '\n');
        this.listBox1.Items.Add((object) "");
        string soapAction = "http://ws.digitalpaytech.com/plateInfo/getValidPlatesByRegion";
        this.listBox1.Items.Add((object) ("SOAPAction: " + soapAction));
        this.listBox1.Items.Add((object) "PLEASE WAIT...");
        this.listBox1.Items.Add((object) "--------------");
        this.listBox1.Items.Add((object) "");
        this.listBox1.Refresh();
        string xml = this.httpPost(theUri, str7, -1, soapAction);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) "REPLY:");
        this.showXml(xml, '<');
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("ERROR " + ex.Message));
        if (ex.InnerException != null)
          this.listBox1.Items.Add((object) ex.InnerException);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) ex.StackTrace);
      }
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    private void lpnButton_Click(object sender, EventArgs e)
    {
      if (!this.checkSandboxSelection() || !this.checkLicSelection())
        return;
      this.listBox1.Items.Clear();
      this.listBox1.Refresh();
      try
      {
        DateTime now = DateTime.Now;
        string[] strArray1 = new string[11];
        string[] strArray2 = strArray1;
        int num = now.Year;
        string str1 = num.ToString("D4");
        strArray2[0] = str1;
        strArray1[1] = "-";
        string[] strArray3 = strArray1;
        num = now.Month;
        string str2 = num.ToString("D2");
        strArray3[2] = str2;
        strArray1[3] = "-";
        string[] strArray4 = strArray1;
        num = now.Day;
        string str3 = num.ToString("D2");
        strArray4[4] = str3;
        strArray1[5] = "T";
        string[] strArray5 = strArray1;
        num = now.Hour;
        string str4 = num.ToString();
        strArray5[6] = str4;
        strArray1[7] = ":";
        string[] strArray6 = strArray1;
        num = now.Minute;
        string str5 = num.ToString();
        strArray6[8] = str5;
        strArray1[9] = ":";
        string[] strArray7 = strArray1;
        num = now.Second;
        string str6 = num.ToString();
        strArray7[10] = str6;
        string str7 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:stal=\"http://ws.digitalpaytech.com/plateInfo\">" + "\r\n<soapenv:Header>" + "\r\n<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" + "\r\n<wsse:UsernameToken wsu:Id=\"UsernameToken-32925455\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" + "\r\n<wsse:Username>" + this.userIdTextBox.Text + "</wsse:Username>" + "\r\n<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + this.passwordTextBox.Text + "</wsse:Password>" + "\r\n<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">xV5+5+AMWvDoqV+eqLZsYQ==</wsse:Nonce>" + "\r\n<wsu:Created>" + string.Concat(strArray1) + "</wsu:Created>" + "\r\n</wsse:UsernameToken>" + "\r\n</wsse:Security>" + "\r\n</soapenv:Header>" + "\r\n<soapenv:Body>" + "\r\n<stal:PlateInfoByPlateRequest>" + "\r\n<token>" + this.token2TextBox.Text + "</token>" + "\r\n<plateNumber>" + this.licTextBox.Text.Trim() + "</plateNumber>" + "\r\n</stal:PlateInfoByPlateRequest>" + "\r\n</soapenv:Body>" + "\r\n</soapenv:Envelope>";
        string theUri = this.urlTextBox.Text + "/PlateInfoService";
        this.listBox1.Items.Add((object) theUri);
        this.listBox1.Items.Add((object) "");
        this.showXml(str7, '\n');
        this.listBox1.Items.Add((object) "");
        string soapAction = "http://ws.digitalpaytech.com/plateInfo/getPlateInfo";
        this.listBox1.Items.Add((object) ("SOAPAction: " + soapAction));
        this.listBox1.Items.Add((object) "PLEASE WAIT...");
        this.listBox1.Items.Add((object) "--------------");
        this.listBox1.Items.Add((object) "");
        this.listBox1.Refresh();
        string xml = this.httpPost(theUri, str7, -1, soapAction);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) "REPLY:");
        this.showXml(xml, '<');
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("ERROR " + ex.Message));
        if (ex.InnerException != null)
          this.listBox1.Items.Add((object) ex.InnerException);
        this.listBox1.Items.Add((object) "");
        this.listBox1.Items.Add((object) ex.StackTrace);
      }
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    public string httpPost(string theUri, string parameters, int timeOut, string soapAction)
    {
      ServicePointManager.CertificatePolicy = (ICertificatePolicy) new Form1.TrustAllCertificatePolicy();
      WebRequest webRequest = WebRequest.Create(theUri);
      webRequest.ContentType = "application/x-www-form-urlencoded";
      webRequest.Method = "POST";
      if (soapAction.Length > 0)
        webRequest.Headers.Add("SOAPAction", soapAction);
      if (timeOut > 0)
        webRequest.Timeout = timeOut;
      byte[] bytes = Encoding.ASCII.GetBytes(parameters);
      webRequest.ContentLength = (long) bytes.Length;
      Stream requestStream = webRequest.GetRequestStream();
      requestStream.Write(bytes, 0, bytes.Length);
      requestStream.Close();
      WebResponse response;
      try
      {
        response = webRequest.GetResponse();
      }
      catch (WebException ex)
      {
        if (ex.Response != null)
        {
          StreamReader streamReader = new StreamReader(ex.Response.GetResponseStream());
          this.listBox1.Items.Add((object) ("httpPost() - " + streamReader.ReadToEnd()));
          this.listBox1.Items.Add((object) ex.Message);
          streamReader.Close();
        }
        throw;
      }
      if (response == null)
        return "";
      StreamReader streamReader1 = new StreamReader(response.GetResponseStream());
      string str = streamReader1.ReadToEnd().Trim();
      streamReader1.Close();
      return str.Replace("&lt;", "<").Replace("&gt;", ">");
    }

    private void showXml(string xml, char sep)
    {
      string str1 = xml;
      char[] chArray = new char[1]{ sep };
      foreach (string str2 in str1.Split(chArray))
        this.listBox1.Items.Add((object) (sep.ToString() + str2));
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.comboBox1.SelectedItem == null)
        return;
      string str = this.comboBox1.SelectedItem.ToString();
      if (str.StartsWith("1"))
      {
        this.urlTextBox.Text = "https://sandbox.digitalpaytech.com/services";
        this.userIdTextBox.Text = "Web Services Demo:WSUser";
        this.passwordTextBox.Text = "WSUserPass";
        this.tokenTextBox.Text = "soHW7kBXBKCrXOQQfICCkOfXdjpgz0SJ";
        this.token2TextBox.Text = "vH3GkjmBz5lfvs33khR6aVVPKyqPBB26";
      }
      else if (str.StartsWith("2"))
      {
        this.urlTextBox.Text = "https://developer.digitalpaytech.com/services";
        this.userIdTextBox.Text = "admin@T2 Systems";
        this.passwordTextBox.Text = "Ab1@222";
        this.tokenTextBox.Text = "WCf9sFARu5jyaNeDFFkEf0KDQddDEsto";
        this.token2TextBox.Text = "CQmmpYdHTXAXvsAO48kr9C4KGGJwpatG";
      }
      else
      {
        this.urlTextBox.Text = "https://?";
        this.userIdTextBox.Text = "";
        this.passwordTextBox.Text = "";
        this.tokenTextBox.Text = "";
      }
    }

    private void helpButton_Click(object sender, EventArgs e)
    {
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Start by selecting a sandbox from the ComboBox.");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "StallInfoService and PlateInfoService may require a different");
      this.listBox1.Items.Add((object) "URL, user ID, token, and password.");
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "Enter a zone code before using Query by Zone commands.");
      this.listBox1.Items.Add((object) "");
    }

    private bool checkSandboxSelection()
    {
      bool flag = true;
      if (this.comboBox1.SelectedItem == null)
        flag = false;
      else if (string.IsNullOrEmpty(this.comboBox1.SelectedItem.ToString()))
        flag = false;
      if (!flag)
      {
        int num = (int) MessageBox.Show("You must select a sandbox.");
      }
      return flag;
    }

    private bool checkZoneSelection()
    {
      if (!string.IsNullOrEmpty(this.zoneCodeTextBox.Text))
        return true;
      int num = (int) MessageBox.Show("You must enter a zone code.");
      return false;
    }

    private bool checkLicSelection()
    {
      if (!string.IsNullOrEmpty(this.licTextBox.Text))
        return true;
      int num = (int) MessageBox.Show("You must enter a license number code.");
      return false;
    }

    public class TrustAllCertificatePolicy : ICertificatePolicy
    {
      public bool CheckValidationResult(
        ServicePoint sp,
        X509Certificate cert,
        WebRequest req,
        int problem)
      {
        return true;
      }
    }
  }
}
