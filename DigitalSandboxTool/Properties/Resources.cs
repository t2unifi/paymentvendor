﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.Properties.Resources
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace DigitalSandboxTool.Properties
{
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (DigitalSandboxTool.Properties.Resources.resourceMan == null)
          DigitalSandboxTool.Properties.Resources.resourceMan = new ResourceManager("DigitalSandboxTool.Properties.Resources", typeof (DigitalSandboxTool.Properties.Resources).Assembly);
        return DigitalSandboxTool.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return DigitalSandboxTool.Properties.Resources.resourceCulture;
      }
      set
      {
        DigitalSandboxTool.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
