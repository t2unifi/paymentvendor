﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.InfoServiceFault
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [XmlRoot(IsNullable = false)]
  [GeneratedCode("System.Runtime.Serialization", "3.0.0.0")]
  [XmlSchemaProvider("ExportSchema")]
  [DebuggerStepThrough]
  [Serializable]
  public class InfoServiceFault : IXmlSerializable, INotifyPropertyChanged
  {
    private static XmlQualifiedName typeName = new XmlQualifiedName(nameof (InfoServiceFault), "http://ws.digitalpaytech.com/plateInfo");
    private XmlNode[] nodesField;

    public XmlNode[] Nodes
    {
      get
      {
        return this.nodesField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.nodesField, (object) value))
          return;
        this.nodesField = value;
        this.RaisePropertyChanged(nameof (Nodes));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void ReadXml(XmlReader reader)
    {
      this.nodesField = XmlSerializableServices.ReadNodes(reader);
    }

    public void WriteXml(XmlWriter writer)
    {
      XmlSerializableServices.WriteNodes(writer, this.Nodes);
    }

    public XmlSchema GetSchema()
    {
      return (XmlSchema) null;
    }

    public static XmlQualifiedName ExportSchema(XmlSchemaSet schemas)
    {
      XmlSerializableServices.AddDefaultSchema(schemas, InfoServiceFault.typeName);
      return InfoServiceFault.typeName;
    }

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
