﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.PlateInfoServiceClient
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [DebuggerStepThrough]
  public class PlateInfoServiceClient : ClientBase<PlateInfoService>, PlateInfoService
  {
    public PlateInfoServiceClient()
    {
    }

    public PlateInfoServiceClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public PlateInfoServiceClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public PlateInfoServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public PlateInfoServiceClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getUpdatedValidPlatesByGroupResponse PlateInfoService.getUpdatedValidPlatesByGroup(
      getUpdatedValidPlatesByGroup request)
    {
      return this.Channel.getUpdatedValidPlatesByGroup(request);
    }

    public PlateInfoType[] getUpdatedValidPlatesByGroup(
      UpdatedPlateInfoByGroupRequest UpdatedPlateInfoByGroupRequest)
    {
      return ((PlateInfoService) this).getUpdatedValidPlatesByGroup(new getUpdatedValidPlatesByGroup()
      {
        UpdatedPlateInfoByGroupRequest = UpdatedPlateInfoByGroupRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getValidPlatesResponse PlateInfoService.getValidPlates(
      getValidPlates request)
    {
      return this.Channel.getValidPlates(request);
    }

    public PlateInfoType[] getValidPlates(PlateInfoRequest PlateInfoRequest)
    {
      return ((PlateInfoService) this).getValidPlates(new getValidPlates()
      {
        PlateInfoRequest = PlateInfoRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getUpdatedValidPlatesByRegionResponse PlateInfoService.getUpdatedValidPlatesByRegion(
      getUpdatedValidPlatesByRegion request)
    {
      return this.Channel.getUpdatedValidPlatesByRegion(request);
    }

    public PlateInfoType[] getUpdatedValidPlatesByRegion(
      UpdatedPlateInfoByRegionRequest UpdatedPlateInfoByRegionRequest)
    {
      return ((PlateInfoService) this).getUpdatedValidPlatesByRegion(new getUpdatedValidPlatesByRegion()
      {
        UpdatedPlateInfoByRegionRequest = UpdatedPlateInfoByRegionRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getValidPlatesByRegionResponse PlateInfoService.getValidPlatesByRegion(
      getValidPlatesByRegion request)
    {
      return this.Channel.getValidPlatesByRegion(request);
    }

    public PlateInfoType[] getValidPlatesByRegion(
      PlateInfoByRegionRequest PlateInfoByRegionRequest)
    {
      return ((PlateInfoService) this).getValidPlatesByRegion(new getValidPlatesByRegion()
      {
        PlateInfoByRegionRequest = PlateInfoByRegionRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getPlateInfoResponse PlateInfoService.getPlateInfo(
      getPlateInfo request)
    {
      return this.Channel.getPlateInfo(request);
    }

    public PlateInfoByPlateResponse getPlateInfo(
      PlateInfoByPlateRequest PlateInfoByPlateRequest)
    {
      return ((PlateInfoService) this).getPlateInfo(new getPlateInfo()
      {
        PlateInfoByPlateRequest = PlateInfoByPlateRequest
      }).PlateInfoByPlateResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getUpdatedValidPlatesResponse PlateInfoService.getUpdatedValidPlates(
      getUpdatedValidPlates request)
    {
      return this.Channel.getUpdatedValidPlates(request);
    }

    public PlateInfoType[] getUpdatedValidPlates(
      UpdatedPlateInfoRequest UpdatedPlateInfoRequest)
    {
      return ((PlateInfoService) this).getUpdatedValidPlates(new getUpdatedValidPlates()
      {
        UpdatedPlateInfoRequest = UpdatedPlateInfoRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getExpiredPlatesByRegionResponse PlateInfoService.getExpiredPlatesByRegion(
      getExpiredPlatesByRegion request)
    {
      return this.Channel.getExpiredPlatesByRegion(request);
    }

    public PlateInfoType[] getExpiredPlatesByRegion(
      ExpiredPlateInfoByRegionRequest ExpiredPlateInfoByRegionRequest)
    {
      return ((PlateInfoService) this).getExpiredPlatesByRegion(new getExpiredPlatesByRegion()
      {
        ExpiredPlateInfoByRegionRequest = ExpiredPlateInfoByRegionRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getExpiredPlatesResponse PlateInfoService.getExpiredPlates(
      getExpiredPlates request)
    {
      return this.Channel.getExpiredPlates(request);
    }

    public PlateInfoType[] getExpiredPlates(
      ExpiredPlateInfoRequest ExpiredPlateInfoRequest)
    {
      return ((PlateInfoService) this).getExpiredPlates(new getExpiredPlates()
      {
        ExpiredPlateInfoRequest = ExpiredPlateInfoRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getValidPlatesByGroupResponse PlateInfoService.getValidPlatesByGroup(
      getValidPlatesByGroup request)
    {
      return this.Channel.getValidPlatesByGroup(request);
    }

    public PlateInfoType[] getValidPlatesByGroup(
      PlateInfoByGroupRequest PlateInfoByGroupRequest)
    {
      return ((PlateInfoService) this).getValidPlatesByGroup(new getValidPlatesByGroup()
      {
        PlateInfoByGroupRequest = PlateInfoByGroupRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getGroupsResponse PlateInfoService.getGroups(getGroups request)
    {
      return this.Channel.getGroups(request);
    }

    public GroupType[] getGroups(GroupRequest GroupRequest)
    {
      return ((PlateInfoService) this).getGroups(new getGroups()
      {
        GroupRequest = GroupRequest
      }).GroupResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getExpiredPlatesByGroupResponse PlateInfoService.getExpiredPlatesByGroup(
      getExpiredPlatesByGroup request)
    {
      return this.Channel.getExpiredPlatesByGroup(request);
    }

    public PlateInfoType[] getExpiredPlatesByGroup(
      ExpiredPlateInfoByGroupRequest ExpiredPlateInfoByGroupRequest)
    {
      return ((PlateInfoService) this).getExpiredPlatesByGroup(new getExpiredPlatesByGroup()
      {
        ExpiredPlateInfoByGroupRequest = ExpiredPlateInfoByGroupRequest
      }).PlateInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getRegionsResponse PlateInfoService.getRegions(
      getRegions request)
    {
      return this.Channel.getRegions(request);
    }

    public RegionType[] getRegions(RegionRequest RegionRequest)
    {
      return ((PlateInfoService) this).getRegions(new getRegions()
      {
        RegionRequest = RegionRequest
      }).RegionResponse;
    }
  }
}
