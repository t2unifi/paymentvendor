﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.getUpdatedValidPlatesResponse
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [MessageContract(IsWrapped = false)]
  [DebuggerStepThrough]
  public class getUpdatedValidPlatesResponse
  {
    [MessageBodyMember(Namespace = "http://ws.digitalpaytech.com/plateInfo", Order = 0)]
    [XmlArrayItem("plateInfos", Form = XmlSchemaForm.Unqualified)]
    public PlateInfoType[] PlateInfoResponse;

    public getUpdatedValidPlatesResponse()
    {
    }

    public getUpdatedValidPlatesResponse(PlateInfoType[] PlateInfoResponse)
    {
      this.PlateInfoResponse = PlateInfoResponse;
    }
  }
}
