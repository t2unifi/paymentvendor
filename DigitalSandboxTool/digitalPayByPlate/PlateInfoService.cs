﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.PlateInfoService
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [ServiceContract(ConfigurationName = "digitalPayByPlate.PlateInfoService", Namespace = "http://ws.digitalpaytech.com/plateInfo")]
  public interface PlateInfoService
  {
    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getUpdatedValidPlatesByGroup", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getUpdatedValidPlatesByGroup", Name = "InfoServiceFault")]
    getUpdatedValidPlatesByGroupResponse getUpdatedValidPlatesByGroup(
      getUpdatedValidPlatesByGroup request);

    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getValidPlates", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getValidPlates", Name = "InfoServiceFault")]
    [XmlSerializerFormat]
    getValidPlatesResponse getValidPlates(getValidPlates request);

    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getUpdatedValidPlatesByRegion", ReplyAction = "*")]
    [XmlSerializerFormat]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getUpdatedValidPlatesByRegion", Name = "InfoServiceFault")]
    getUpdatedValidPlatesByRegionResponse getUpdatedValidPlatesByRegion(
      getUpdatedValidPlatesByRegion request);

    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getValidPlatesByRegion", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getValidPlatesByRegion", Name = "InfoServiceFault")]
    [XmlSerializerFormat]
    getValidPlatesByRegionResponse getValidPlatesByRegion(
      getValidPlatesByRegion request);

    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getPlateInfo", Name = "InfoServiceFault")]
    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getPlateInfo", ReplyAction = "*")]
    getPlateInfoResponse getPlateInfo(getPlateInfo request);

    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getUpdatedValidPlates", ReplyAction = "*")]
    [XmlSerializerFormat]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getUpdatedValidPlates", Name = "InfoServiceFault")]
    getUpdatedValidPlatesResponse getUpdatedValidPlates(
      getUpdatedValidPlates request);

    [XmlSerializerFormat]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getExpiredPlatesByRegion", Name = "InfoServiceFault")]
    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getExpiredPlatesByRegion", ReplyAction = "*")]
    getExpiredPlatesByRegionResponse getExpiredPlatesByRegion(
      getExpiredPlatesByRegion request);

    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getExpiredPlates", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getExpiredPlates", Name = "InfoServiceFault")]
    [XmlSerializerFormat]
    getExpiredPlatesResponse getExpiredPlates(getExpiredPlates request);

    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getValidPlatesByGroup", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getValidPlatesByGroup", Name = "InfoServiceFault")]
    getValidPlatesByGroupResponse getValidPlatesByGroup(
      getValidPlatesByGroup request);

    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getGroups", Name = "InfoServiceFault")]
    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getGroups", ReplyAction = "*")]
    getGroupsResponse getGroups(getGroups request);

    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getExpiredPlatesByGroup", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getExpiredPlatesByGroup", Name = "InfoServiceFault")]
    getExpiredPlatesByGroupResponse getExpiredPlatesByGroup(
      getExpiredPlatesByGroup request);

    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/plateInfo/getRegions", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/plateInfo/getRegions", Name = "InfoServiceFault")]
    getRegionsResponse getRegions(getRegions request);
  }
}
