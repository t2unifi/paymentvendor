﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.PlateInfoType
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ws.digitalpaytech.com/plateInfo")]
  [Serializable]
  public class PlateInfoType : INotifyPropertyChanged
  {
    private DateTime expiryDateField;
    private DateTime purchasedDateField;
    private string plateNumberField;
    private string regionNameField;
    private StatusType statusField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public DateTime expiryDate
    {
      get
      {
        return this.expiryDateField;
      }
      set
      {
        this.expiryDateField = value;
        this.RaisePropertyChanged(nameof (expiryDate));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public DateTime purchasedDate
    {
      get
      {
        return this.purchasedDateField;
      }
      set
      {
        this.purchasedDateField = value;
        this.RaisePropertyChanged(nameof (purchasedDate));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public string plateNumber
    {
      get
      {
        return this.plateNumberField;
      }
      set
      {
        this.plateNumberField = value;
        this.RaisePropertyChanged(nameof (plateNumber));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
    public string regionName
    {
      get
      {
        return this.regionNameField;
      }
      set
      {
        this.regionNameField = value;
        this.RaisePropertyChanged(nameof (regionName));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
    public StatusType status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
        this.RaisePropertyChanged(nameof (status));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
