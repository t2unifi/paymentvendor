﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.getValidPlates
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [DebuggerStepThrough]
  [MessageContract(IsWrapped = false)]
  public class getValidPlates
  {
    [MessageBodyMember(Namespace = "http://ws.digitalpaytech.com/plateInfo", Order = 0)]
    public PlateInfoRequest PlateInfoRequest;

    public getValidPlates()
    {
    }

    public getValidPlates(PlateInfoRequest PlateInfoRequest)
    {
      this.PlateInfoRequest = PlateInfoRequest;
    }
  }
}
