﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.getGroupsResponse
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [MessageContract(IsWrapped = false)]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [DebuggerStepThrough]
  public class getGroupsResponse
  {
    [XmlArrayItem("groups", Form = XmlSchemaForm.Unqualified)]
    [MessageBodyMember(Namespace = "http://ws.digitalpaytech.com/plateInfo", Order = 0)]
    public GroupType[] GroupResponse;

    public getGroupsResponse()
    {
    }

    public getGroupsResponse(GroupType[] GroupResponse)
    {
      this.GroupResponse = GroupResponse;
    }
  }
}
