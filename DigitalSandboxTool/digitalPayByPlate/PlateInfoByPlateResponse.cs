﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayByPlate.PlateInfoByPlateResponse
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayByPlate
{
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true, Namespace = "http://ws.digitalpaytech.com/plateInfo")]
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [DebuggerStepThrough]
  [Serializable]
  public class PlateInfoByPlateResponse : INotifyPropertyChanged
  {
    private PlateInfoType plateInfoField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public PlateInfoType plateInfo
    {
      get
      {
        return this.plateInfoField;
      }
      set
      {
        this.plateInfoField = value;
        this.RaisePropertyChanged(nameof (plateInfo));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
