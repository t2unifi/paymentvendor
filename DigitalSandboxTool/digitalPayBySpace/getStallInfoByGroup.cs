﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.getStallInfoByGroup
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [MessageContract(IsWrapped = false)]
  [DebuggerStepThrough]
  public class getStallInfoByGroup
  {
    [MessageBodyMember(Namespace = "http://ws.digitalpioneer.com/stallInfo", Order = 0)]
    public StallInfoByGroupRequest StallInfoByGroupRequest;

    public getStallInfoByGroup()
    {
    }

    public getStallInfoByGroup(StallInfoByGroupRequest StallInfoByGroupRequest)
    {
      this.StallInfoByGroupRequest = StallInfoByGroupRequest;
    }
  }
}
