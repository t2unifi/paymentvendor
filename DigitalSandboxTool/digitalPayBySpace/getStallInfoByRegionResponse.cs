﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.getStallInfoByRegionResponse
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [MessageContract(IsWrapped = false)]
  public class getStallInfoByRegionResponse
  {
    [MessageBodyMember(Namespace = "http://ws.digitalpioneer.com/stallInfo", Order = 0)]
    [XmlArrayItem("stallInfos", Form = XmlSchemaForm.Unqualified)]
    public StallInfoType[] StallInfoResponse;

    public getStallInfoByRegionResponse()
    {
    }

    public getStallInfoByRegionResponse(StallInfoType[] StallInfoResponse)
    {
      this.StallInfoResponse = StallInfoResponse;
    }
  }
}
