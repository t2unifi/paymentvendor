﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.StallInfoService
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [ServiceContract(ConfigurationName = "digitalPayBySpace.StallInfoService", Namespace = "http://ws.digitalpioneer.com/stallInfo")]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  public interface StallInfoService
  {
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpioneer.com/stallInfo/getRegions", Name = "InfoServiceFault")]
    [OperationContract(Action = "http://ws.digitalpioneer.com/stallInfo/getRegions", ReplyAction = "*")]
    [XmlSerializerFormat]
    getRegionsResponse getRegions(getRegions request);

    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfoByGroup", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfoByGroup", Name = "InfoServiceFault")]
    getStallInfoByGroupResponse getStallInfoByGroup(
      getStallInfoByGroup request);

    [OperationContract(Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfoByRegion", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfoByRegion", Name = "InfoServiceFault")]
    [XmlSerializerFormat]
    getStallInfoByRegionResponse getStallInfoByRegion(
      getStallInfoByRegion request);

    [OperationContract(Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfoBySetting", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfoBySetting", Name = "InfoServiceFault")]
    [XmlSerializerFormat]
    getStallInfoBySettingResponse getStallInfoBySetting(
      getStallInfoBySetting request);

    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpioneer.com/stallInfo/getGroups", Name = "InfoServiceFault")]
    [OperationContract(Action = "http://ws.digitalpioneer.com/stallInfo/getGroups", ReplyAction = "*")]
    [XmlSerializerFormat]
    getGroupsResponse getGroups(getGroups request);

    [XmlSerializerFormat]
    [OperationContract(Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfo", ReplyAction = "*")]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpaytech.com/stallInfo/getStallInfo", Name = "InfoServiceFault")]
    getStallInfoResponse getStallInfo(getStallInfo request);

    [OperationContract(Action = "http://ws.digitalpioneer.com/stallInfo/getSettings", ReplyAction = "*")]
    [XmlSerializerFormat]
    [FaultContract(typeof (InfoServiceFault), Action = "http://ws.digitalpioneer.com/stallInfo/getSettings", Name = "InfoServiceFault")]
    getSettingsResponse getSettings(getSettings request);
  }
}
