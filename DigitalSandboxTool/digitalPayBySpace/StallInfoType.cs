﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.StallInfoType
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [XmlType(Namespace = "http://ws.digitalpioneer.com/stallInfo")]
  [Serializable]
  public class StallInfoType : INotifyPropertyChanged
  {
    private DateTime expiryDateField;
    private DateTime purchaseDateField;
    private string stallNumberField;
    private string settingField;
    private string statusField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public DateTime expiryDate
    {
      get
      {
        return this.expiryDateField;
      }
      set
      {
        this.expiryDateField = value;
        this.RaisePropertyChanged(nameof (expiryDate));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public DateTime purchaseDate
    {
      get
      {
        return this.purchaseDateField;
      }
      set
      {
        this.purchaseDateField = value;
        this.RaisePropertyChanged(nameof (purchaseDate));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public string stallNumber
    {
      get
      {
        return this.stallNumberField;
      }
      set
      {
        this.stallNumberField = value;
        this.RaisePropertyChanged(nameof (stallNumber));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
    public string setting
    {
      get
      {
        return this.settingField;
      }
      set
      {
        this.settingField = value;
        this.RaisePropertyChanged(nameof (setting));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
    public string status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
        this.RaisePropertyChanged(nameof (status));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
