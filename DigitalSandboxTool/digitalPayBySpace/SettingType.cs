﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.SettingType
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://ws.digitalpioneer.com/stallInfo")]
  [Serializable]
  public class SettingType : INotifyPropertyChanged
  {
    private string settingNameField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public string settingName
    {
      get
      {
        return this.settingNameField;
      }
      set
      {
        this.settingNameField = value;
        this.RaisePropertyChanged(nameof (settingName));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
