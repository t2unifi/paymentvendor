﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.StallInfoByRegionRequest
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [DebuggerStepThrough]
  [XmlType(AnonymousType = true, Namespace = "http://ws.digitalpioneer.com/stallInfo")]
  [Serializable]
  public class StallInfoByRegionRequest : INotifyPropertyChanged
  {
    private string tokenField;
    private string regionField;
    private int stallfromField;
    private int stalltoField;
    private Bystatus stallstatusField;
    private DateTime datetimeStampField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public string token
    {
      get
      {
        return this.tokenField;
      }
      set
      {
        this.tokenField = value;
        this.RaisePropertyChanged(nameof (token));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public string region
    {
      get
      {
        return this.regionField;
      }
      set
      {
        this.regionField = value;
        this.RaisePropertyChanged(nameof (region));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public int stallfrom
    {
      get
      {
        return this.stallfromField;
      }
      set
      {
        this.stallfromField = value;
        this.RaisePropertyChanged(nameof (stallfrom));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
    public int stallto
    {
      get
      {
        return this.stalltoField;
      }
      set
      {
        this.stalltoField = value;
        this.RaisePropertyChanged(nameof (stallto));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
    public Bystatus stallstatus
    {
      get
      {
        return this.stallstatusField;
      }
      set
      {
        this.stallstatusField = value;
        this.RaisePropertyChanged(nameof (stallstatus));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 5)]
    public DateTime datetimeStamp
    {
      get
      {
        return this.datetimeStampField;
      }
      set
      {
        this.datetimeStampField = value;
        this.RaisePropertyChanged(nameof (datetimeStamp));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
