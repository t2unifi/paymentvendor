﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.Bystatus
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [XmlType(Namespace = "http://ws.digitalpioneer.com/stallInfo")]
  [Serializable]
  public enum Bystatus
  {
    All,
    Valid,
    Expired,
  }
}
