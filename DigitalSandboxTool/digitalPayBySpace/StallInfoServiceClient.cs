﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.StallInfoServiceClient
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  public class StallInfoServiceClient : ClientBase<StallInfoService>, StallInfoService
  {
    public StallInfoServiceClient()
    {
    }

    public StallInfoServiceClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public StallInfoServiceClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public StallInfoServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public StallInfoServiceClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getRegionsResponse StallInfoService.getRegions(
      getRegions request)
    {
      return this.Channel.getRegions(request);
    }

    public RegionType[] getRegions(RegionRequest RegionRequest)
    {
      return ((StallInfoService) this).getRegions(new getRegions()
      {
        RegionRequest = RegionRequest
      }).RegionResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getStallInfoByGroupResponse StallInfoService.getStallInfoByGroup(
      getStallInfoByGroup request)
    {
      return this.Channel.getStallInfoByGroup(request);
    }

    public StallInfoType[] getStallInfoByGroup(
      StallInfoByGroupRequest StallInfoByGroupRequest)
    {
      return ((StallInfoService) this).getStallInfoByGroup(new getStallInfoByGroup()
      {
        StallInfoByGroupRequest = StallInfoByGroupRequest
      }).StallInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getStallInfoByRegionResponse StallInfoService.getStallInfoByRegion(
      getStallInfoByRegion request)
    {
      return this.Channel.getStallInfoByRegion(request);
    }

    public StallInfoType[] getStallInfoByRegion(
      StallInfoByRegionRequest StallInfoByRegionRequest)
    {
      return ((StallInfoService) this).getStallInfoByRegion(new getStallInfoByRegion()
      {
        StallInfoByRegionRequest = StallInfoByRegionRequest
      }).StallInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getStallInfoBySettingResponse StallInfoService.getStallInfoBySetting(
      getStallInfoBySetting request)
    {
      return this.Channel.getStallInfoBySetting(request);
    }

    public StallInfoType[] getStallInfoBySetting(
      StallInfoBySettingRequest StallInfoBySettingRequest)
    {
      return ((StallInfoService) this).getStallInfoBySetting(new getStallInfoBySetting()
      {
        StallInfoBySettingRequest = StallInfoBySettingRequest
      }).StallInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getGroupsResponse StallInfoService.getGroups(getGroups request)
    {
      return this.Channel.getGroups(request);
    }

    public GroupType[] getGroups(GroupRequest GroupRequest)
    {
      return ((StallInfoService) this).getGroups(new getGroups()
      {
        GroupRequest = GroupRequest
      }).GroupResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getStallInfoResponse StallInfoService.getStallInfo(
      getStallInfo request)
    {
      return this.Channel.getStallInfo(request);
    }

    public StallInfoType[] getStallInfo(StallInfoRequest StallInfoRequest)
    {
      return ((StallInfoService) this).getStallInfo(new getStallInfo()
      {
        StallInfoRequest = StallInfoRequest
      }).StallInfoResponse;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getSettingsResponse StallInfoService.getSettings(
      getSettings request)
    {
      return this.Channel.getSettings(request);
    }

    public SettingType[] getSettings(SettingRequest SettingRequest)
    {
      return ((StallInfoService) this).getSettings(new getSettings()
      {
        SettingRequest = SettingRequest
      }).SettingResponse;
    }
  }
}
