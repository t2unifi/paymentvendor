﻿// Decompiled with JetBrains decompiler
// Type: DigitalSandboxTool.digitalPayBySpace.getStallInfoBySetting
// Assembly: DigitalSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2F9A16E-C8F8-47F0-8CE0-E44ACAF2B0BE
// Assembly location: C:\SWAN\MEA\Digital Sandbox Tool 2014 02 12\DigitalSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace DigitalSandboxTool.digitalPayBySpace
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [MessageContract(IsWrapped = false)]
  public class getStallInfoBySetting
  {
    [MessageBodyMember(Namespace = "http://ws.digitalpioneer.com/stallInfo", Order = 0)]
    public StallInfoBySettingRequest StallInfoBySettingRequest;

    public getStallInfoBySetting()
    {
    }

    public getStallInfoBySetting(
      StallInfoBySettingRequest StallInfoBySettingRequest)
    {
      this.StallInfoBySettingRequest = StallInfoBySettingRequest;
    }
  }
}
