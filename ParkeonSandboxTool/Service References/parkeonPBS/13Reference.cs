﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.getOccupancyResponse
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ParkeonSandboxTool.parkeonPBS
{
  [MessageContract(IsWrapped = true, WrapperName = "getOccupancyResponse", WrapperNamespace = "http://enforcement.paybyspace.parkeon.com/")]
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  public class getOccupancyResponse
  {
    [MessageBodyMember(Namespace = "http://enforcement.paybyspace.parkeon.com/", Order = 0)]
    [XmlArray(Form = XmlSchemaForm.Unqualified)]
    [XmlArrayItem("statuses", Form = XmlSchemaForm.Unqualified)]
    public spaceStatus[] spaceNumbersAndStatuses;

    public getOccupancyResponse()
    {
    }

    public getOccupancyResponse(spaceStatus[] spaceNumbersAndStatuses)
    {
      this.spaceNumbersAndStatuses = spaceNumbersAndStatuses;
    }
  }
}
