﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.mapEntry
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ParkeonSandboxTool.parkeonPBS
{
  [XmlType(AnonymousType = true, Namespace = "http://enforcement.paybyspace.parkeon.com/")]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [DebuggerStepThrough]
  [Serializable]
  public class mapEntry : INotifyPropertyChanged
  {
    private int keyField;
    private bool keyFieldSpecified;
    private string valueField;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public int key
    {
      get
      {
        return this.keyField;
      }
      set
      {
        this.keyField = value;
        this.RaisePropertyChanged(nameof (key));
      }
    }

    [XmlIgnore]
    public bool keySpecified
    {
      get
      {
        return this.keyFieldSpecified;
      }
      set
      {
        this.keyFieldSpecified = value;
        this.RaisePropertyChanged(nameof (keySpecified));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public string value
    {
      get
      {
        return this.valueField;
      }
      set
      {
        this.valueField = value;
        this.RaisePropertyChanged(nameof (value));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
