﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.EnforcementClient
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace ParkeonSandboxTool.parkeonPBS
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [DebuggerStepThrough]
  public class EnforcementClient : ClientBase<Enforcement>, Enforcement
  {
    public EnforcementClient()
    {
    }

    public EnforcementClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public EnforcementClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public EnforcementClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public EnforcementClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    startEnforcementResponse Enforcement.startEnforcement(
      startEnforcementRequest request)
    {
      return this.Channel.startEnforcement(request);
    }

    public int startEnforcement(int controlAreaId)
    {
      return ((Enforcement) this).startEnforcement(new startEnforcementRequest()
      {
        controlAreaId = controlAreaId
      }).enforcementSessionId;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    stopEnforcementResponse Enforcement.stopEnforcement(
      stopEnforcementRequest request)
    {
      return this.Channel.stopEnforcement(request);
    }

    public void stopEnforcement(int enforcementSessionId)
    {
      ((Enforcement) this).stopEnforcement(new stopEnforcementRequest()
      {
        enforcementSessionId = enforcementSessionId
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getControlAreasResponse Enforcement.getControlAreas(
      getControlAreasRequest request)
    {
      return this.Channel.getControlAreas(request);
    }

    public map getControlAreas(int parkId)
    {
      return ((Enforcement) this).getControlAreas(new getControlAreasRequest()
      {
        parkId = parkId
      }).controlAreasIdsAndNames;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getParksResponse Enforcement.getParks(getParksRequest request)
    {
      return this.Channel.getParks(request);
    }

    public map getParks()
    {
      return ((Enforcement) this).getParks(new getParksRequest()).parksIdsAndNames;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getOccupancyResponse Enforcement.getOccupancy(
      getOccupancyRequest request)
    {
      return this.Channel.getOccupancy(request);
    }

    public spaceStatus[] getOccupancy(int enforcementSessionId)
    {
      return ((Enforcement) this).getOccupancy(new getOccupancyRequest()
      {
        enforcementSessionId = enforcementSessionId
      }).spaceNumbersAndStatuses;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    getNonCommunicatingMetersResponse Enforcement.getNonCommunicatingMeters(
      getNonCommunicatingMetersRequest request)
    {
      return this.Channel.getNonCommunicatingMeters(request);
    }

    public map getNonCommunicatingMeters(int enforcementSessionId)
    {
      return ((Enforcement) this).getNonCommunicatingMeters(new getNonCommunicatingMetersRequest()
      {
        enforcementSessionId = enforcementSessionId
      }).metersCodesAndDescriptions;
    }
  }
}
