﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.startEnforcementResponse
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ParkeonSandboxTool.parkeonPBS
{
  [DebuggerStepThrough]
  [MessageContract(IsWrapped = true, WrapperName = "startEnforcementResponse", WrapperNamespace = "http://enforcement.paybyspace.parkeon.com/")]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  public class startEnforcementResponse
  {
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    [MessageBodyMember(Namespace = "http://enforcement.paybyspace.parkeon.com/", Order = 0)]
    public int enforcementSessionId;

    public startEnforcementResponse()
    {
    }

    public startEnforcementResponse(int enforcementSessionId)
    {
      this.enforcementSessionId = enforcementSessionId;
    }
  }
}
