﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.map
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ParkeonSandboxTool.parkeonPBS
{
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://enforcement.paybyspace.parkeon.com/")]
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [Serializable]
  public class map : INotifyPropertyChanged
  {
    private mapEntry[] idsAndNamesField;

    [XmlArrayItem("entry", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
    [XmlArray(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public mapEntry[] idsAndNames
    {
      get
      {
        return this.idsAndNamesField;
      }
      set
      {
        this.idsAndNamesField = value;
        this.RaisePropertyChanged(nameof (idsAndNames));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
