﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.PayBySpaceException
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ParkeonSandboxTool.parkeonPBS
{
  [GeneratedCode("System.Runtime.Serialization", "3.0.0.0")]
  [XmlSchemaProvider("ExportSchema")]
  [XmlRoot(IsNullable = false)]
  [DebuggerStepThrough]
  [Serializable]
  public class PayBySpaceException : IXmlSerializable, INotifyPropertyChanged
  {
    private static XmlQualifiedName typeName = new XmlQualifiedName(nameof (PayBySpaceException), "http://enforcement.paybyspace.parkeon.com/");
    private XmlNode[] nodesField;

    public XmlNode[] Nodes
    {
      get
      {
        return this.nodesField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.nodesField, (object) value))
          return;
        this.nodesField = value;
        this.RaisePropertyChanged(nameof (Nodes));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void ReadXml(XmlReader reader)
    {
      this.nodesField = XmlSerializableServices.ReadNodes(reader);
    }

    public void WriteXml(XmlWriter writer)
    {
      XmlSerializableServices.WriteNodes(writer, this.Nodes);
    }

    public XmlSchema GetSchema()
    {
      return (XmlSchema) null;
    }

    public static XmlQualifiedName ExportSchema(XmlSchemaSet schemas)
    {
      XmlSerializableServices.AddDefaultSchema(schemas, PayBySpaceException.typeName);
      return PayBySpaceException.typeName;
    }

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
