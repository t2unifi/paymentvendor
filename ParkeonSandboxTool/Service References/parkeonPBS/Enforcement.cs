﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.Enforcement
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace ParkeonSandboxTool.parkeonPBS
{
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [ServiceContract(ConfigurationName = "parkeonPBS.Enforcement", Namespace = "http://enforcement.paybyspace.parkeon.com/")]
  public interface Enforcement
  {
    [FaultContract(typeof (PayBySpaceException), Action = "", Name = "PayBySpaceException")]
    [OperationContract(Action = "", ReplyAction = "*")]
    [XmlSerializerFormat]
    [return: MessageParameter(Name = "enforcementSessionId")]
    startEnforcementResponse startEnforcement(
      startEnforcementRequest request);

    [XmlSerializerFormat]
    [OperationContract(Action = "", ReplyAction = "*")]
    [FaultContract(typeof (PayBySpaceException), Action = "", Name = "PayBySpaceException")]
    stopEnforcementResponse stopEnforcement(stopEnforcementRequest request);

    [XmlSerializerFormat]
    [OperationContract(Action = "", ReplyAction = "*")]
    [return: MessageParameter(Name = "controlAreasIdsAndNames")]
    getControlAreasResponse getControlAreas(getControlAreasRequest request);

    [FaultContract(typeof (PayBySpaceException), Action = "", Name = "PayBySpaceException")]
    [XmlSerializerFormat]
    [OperationContract(Action = "", ReplyAction = "*")]
    [return: MessageParameter(Name = "parksIdsAndNames")]
    getParksResponse getParks(getParksRequest request);

    [FaultContract(typeof (PayBySpaceException), Action = "", Name = "PayBySpaceException")]
    [OperationContract(Action = "", ReplyAction = "*")]
    [XmlSerializerFormat]
    [return: MessageParameter(Name = "spaceNumbersAndStatuses")]
    getOccupancyResponse getOccupancy(getOccupancyRequest request);

    [OperationContract(Action = "", ReplyAction = "*")]
    [FaultContract(typeof (PayBySpaceException), Action = "", Name = "PayBySpaceException")]
    [XmlSerializerFormat]
    [return: MessageParameter(Name = "metersCodesAndDescriptions")]
    getNonCommunicatingMetersResponse getNonCommunicatingMeters(
      getNonCommunicatingMetersRequest request);
  }
}
