﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.spaceStatus
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ParkeonSandboxTool.parkeonPBS
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "2.0.50727.3082")]
  [XmlType(Namespace = "http://enforcement.paybyspace.parkeon.com/")]
  [Serializable]
  public class spaceStatus : INotifyPropertyChanged
  {
    private string statusField;
    private string spaceNumberField;
    private DateTime expirationDateField;
    private bool expirationDateFieldSpecified;

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
    public string status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
        this.RaisePropertyChanged(nameof (status));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
    public string spaceNumber
    {
      get
      {
        return this.spaceNumberField;
      }
      set
      {
        this.spaceNumberField = value;
        this.RaisePropertyChanged(nameof (spaceNumber));
      }
    }

    [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
    public DateTime expirationDate
    {
      get
      {
        return this.expirationDateField;
      }
      set
      {
        this.expirationDateField = value;
        this.RaisePropertyChanged(nameof (expirationDate));
      }
    }

    [XmlIgnore]
    public bool expirationDateSpecified
    {
      get
      {
        return this.expirationDateFieldSpecified;
      }
      set
      {
        this.expirationDateFieldSpecified = value;
        this.RaisePropertyChanged(nameof (expirationDateSpecified));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
