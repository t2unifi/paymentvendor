﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.parkeonPBS.getOccupancyRequest
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ParkeonSandboxTool.parkeonPBS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "3.0.0.0")]
  [MessageContract(IsWrapped = true, WrapperName = "getOccupancy", WrapperNamespace = "http://enforcement.paybyspace.parkeon.com/")]
  public class getOccupancyRequest
  {
    [MessageBodyMember(Namespace = "http://enforcement.paybyspace.parkeon.com/", Order = 0)]
    [XmlElement(Form = XmlSchemaForm.Unqualified)]
    public int enforcementSessionId;

    public getOccupancyRequest()
    {
    }

    public getOccupancyRequest(int enforcementSessionId)
    {
      this.enforcementSessionId = enforcementSessionId;
    }
  }
}
