﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.Form1
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using ParkeonSandboxTool.parkeonPBS;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ParkeonSandboxTool
{
  public class Form1 : Form
  {
    private IContainer components = (IContainer) null;
    private Button queryByZoneButton;
    private ListBox listBox1;
    private Label label1;
    private TextBox userIdTextBox;
    private TextBox zoneCodeTextBox;
    private Label label2;
    private TextBox passwordTextBox;
    private Label label4;
    private Button helpButton;
    private Button listControlAreasButton;
    private ComboBox urlComboBox;
    private Label label3;
    private TextBox urlTextBox;

    public Form1()
    {
      this.InitializeComponent();
      this.userIdTextBox.Text = "T2";
      this.zoneCodeTextBox.Text = "";
      this.passwordTextBox.Text = "testT2";
      this.urlComboBox.SelectedIndex = 0;
    }

    private void queryByZoneButton_Click(object sender, EventArgs e)
    {
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Please wait...");
      this.listBox1.Refresh();
      try
      {
        string text1 = this.urlTextBox.Text;
        string text2 = this.userIdTextBox.Text;
        string text3 = this.passwordTextBox.Text;
        this.listBox1.Items.Add((object) ("URL: " + text1));
        this.listBox1.Items.Add((object) ("User: " + text2 + " Password: " + text3));
        EnforcementClient enforcementClient = new EnforcementClient("EnforcementPort", text1);
        enforcementClient.ClientCredentials.UserName.UserName = text2;
        enforcementClient.ClientCredentials.UserName.Password = text3;
        int int32;
        try
        {
          int32 = Convert.ToInt32(this.zoneCodeTextBox.Text);
          this.listBox1.Items.Add((object) ("Control Area: " + int32.ToString()));
        }
        catch
        {
          this.listBox1.Items.Add((object) "Control area must be numeric.");
          return;
        }
        int enforcementSessionId = enforcementClient.startEnforcement(int32);
        foreach (mapEntry idsAndName in enforcementClient.getNonCommunicatingMeters(enforcementSessionId).idsAndNames)
          this.listBox1.Items.Add((object) ("Meter: " + (object) idsAndName.key + " not responding."));
        foreach (spaceStatus spaceStatus in enforcementClient.getOccupancy(enforcementSessionId))
          this.listBox1.Items.Add((object) ("Space: " + spaceStatus.spaceNumber + " Status: " + spaceStatus.status.Replace("_", " ") + " Expires: " + spaceStatus.expirationDate.ToString("o")));
        enforcementClient.stopEnforcement(enforcementSessionId);
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("Exception: " + ex.Message));
        if (ex.InnerException != null)
          this.listBox1.Items.Add((object) ("Inner Exception: " + ex.InnerException.Message));
        this.listBox1.Items.Add((object) ("Stack Trace: " + ex.StackTrace));
      }
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    private void listControlAreasButton_Click(object sender, EventArgs e)
    {
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "Please wait...");
      this.listBox1.Refresh();
      try
      {
        string text1 = this.urlTextBox.Text;
        string text2 = this.userIdTextBox.Text;
        string text3 = this.passwordTextBox.Text;
        this.listBox1.Items.Add((object) ("URL: " + text1));
        this.listBox1.Items.Add((object) ("User: " + text2 + " Password: " + text3));
        EnforcementClient enforcementClient = new EnforcementClient("EnforcementPort", text1);
        enforcementClient.ClientCredentials.UserName.UserName = text2;
        enforcementClient.ClientCredentials.UserName.Password = text3;
        foreach (mapEntry idsAndName1 in enforcementClient.getParks().idsAndNames)
        {
          this.listBox1.Items.Add((object) ("Park - id:" + (object) idsAndName1.key + " name:" + idsAndName1.value));
          foreach (mapEntry idsAndName2 in enforcementClient.getControlAreas(idsAndName1.key).idsAndNames)
            this.listBox1.Items.Add((object) ("Control Area - id:" + (object) idsAndName2.key + ", name:" + idsAndName2.value));
        }
      }
      catch (Exception ex)
      {
        this.listBox1.Items.Add((object) ("Exception: " + ex.Message));
        if (ex.InnerException != null)
          this.listBox1.Items.Add((object) ("Inner Exception: " + ex.InnerException.Message));
        this.listBox1.Items.Add((object) ("Stack Trace: " + ex.StackTrace));
      }
      this.listBox1.Items.Add((object) "--- End of List ---");
    }

    private void helpButton_Click(object sender, EventArgs e)
    {
      this.listBox1.Items.Clear();
      this.listBox1.Items.Add((object) "");
      this.listBox1.Items.Add((object) "");
    }

    private void urlComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.urlTextBox.Text = this.urlComboBox.SelectedItem.ToString();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.queryByZoneButton = new Button();
      this.listBox1 = new ListBox();
      this.label1 = new Label();
      this.userIdTextBox = new TextBox();
      this.zoneCodeTextBox = new TextBox();
      this.label2 = new Label();
      this.passwordTextBox = new TextBox();
      this.label4 = new Label();
      this.helpButton = new Button();
      this.listControlAreasButton = new Button();
      this.urlComboBox = new ComboBox();
      this.label3 = new Label();
      this.urlTextBox = new TextBox();
      this.SuspendLayout();
      this.queryByZoneButton.Location = new Point(16, 512);
      this.queryByZoneButton.Margin = new Padding(4, 4, 4, 4);
      this.queryByZoneButton.Name = "queryByZoneButton";
      this.queryByZoneButton.Size = new Size(271, 28);
      this.queryByZoneButton.TabIndex = 0;
      this.queryByZoneButton.Text = "Query Parking Rights by Zone";
      this.queryByZoneButton.UseVisualStyleBackColor = true;
      this.queryByZoneButton.Click += new EventHandler(this.queryByZoneButton_Click);
      this.listBox1.FormattingEnabled = true;
      this.listBox1.ItemHeight = 16;
      this.listBox1.Location = new Point(16, 101);
      this.listBox1.Margin = new Padding(4, 4, 4, 4);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(1189, 388);
      this.listBox1.TabIndex = 1;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 11);
      this.label1.Margin = new Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(59, 17);
      this.label1.TabIndex = 2;
      this.label1.Text = "User ID:";
      this.userIdTextBox.Location = new Point(103, 7);
      this.userIdTextBox.Margin = new Padding(4, 4, 4, 4);
      this.userIdTextBox.Name = "userIdTextBox";
      this.userIdTextBox.Size = new Size(143, 22);
      this.userIdTextBox.TabIndex = 3;
      this.zoneCodeTextBox.Location = new Point(444, 7);
      this.zoneCodeTextBox.Margin = new Padding(4, 4, 4, 4);
      this.zoneCodeTextBox.Name = "zoneCodeTextBox";
      this.zoneCodeTextBox.Size = new Size(145, 22);
      this.zoneCodeTextBox.TabIndex = 5;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(265, 11);
      this.label2.Margin = new Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new Size(175, 17);
      this.label2.TabIndex = 4;
      this.label2.Text = "Zone Code (Control Area):";
      this.passwordTextBox.Location = new Point(103, 39);
      this.passwordTextBox.Margin = new Padding(4, 4, 4, 4);
      this.passwordTextBox.Name = "passwordTextBox";
      this.passwordTextBox.Size = new Size(143, 22);
      this.passwordTextBox.TabIndex = 9;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(12, 43);
      this.label4.Margin = new Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new Size(73, 17);
      this.label4.TabIndex = 8;
      this.label4.Text = "Password:";
      this.helpButton.Location = new Point(1095, 512);
      this.helpButton.Margin = new Padding(4, 4, 4, 4);
      this.helpButton.Name = "helpButton";
      this.helpButton.Size = new Size(112, 28);
      this.helpButton.TabIndex = 23;
      this.helpButton.Text = "Help";
      this.helpButton.UseVisualStyleBackColor = true;
      this.helpButton.Click += new EventHandler(this.helpButton_Click);
      this.listControlAreasButton.Location = new Point(332, 512);
      this.listControlAreasButton.Margin = new Padding(4, 4, 4, 4);
      this.listControlAreasButton.Name = "listControlAreasButton";
      this.listControlAreasButton.Size = new Size(180, 28);
      this.listControlAreasButton.TabIndex = 25;
      this.listControlAreasButton.Text = "List Control Areas";
      this.listControlAreasButton.UseVisualStyleBackColor = true;
      this.listControlAreasButton.Click += new EventHandler(this.listControlAreasButton_Click);
      this.urlComboBox.FormattingEnabled = true;
      this.urlComboBox.Items.AddRange(new object[2]
      {
        (object) "http://pkf.us.parkfolio.com:8080/PayBySpaceWebServices/EnforcementService",
        (object) "http://pbs-pp.parkfolio.com/PayBySpaceWebServices/EnforcementService"
      });
      this.urlComboBox.Location = new Point(444, 71);
      this.urlComboBox.Margin = new Padding(4, 4, 4, 4);
      this.urlComboBox.Name = "urlComboBox";
      this.urlComboBox.Size = new Size(600, 24);
      this.urlComboBox.TabIndex = 26;
      this.urlComboBox.SelectedIndexChanged += new EventHandler(this.urlComboBox_SelectedIndexChanged);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(380, 43);
      this.label3.Margin = new Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new Size(40, 17);
      this.label3.TabIndex = 27;
      this.label3.Text = "URL:";
      this.urlTextBox.Location = new Point(444, 42);
      this.urlTextBox.Name = "urlTextBox";
      this.urlTextBox.Size = new Size(600, 22);
      this.urlTextBox.TabIndex = 28;
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(1223, 554);
      this.Controls.Add((Control) this.urlTextBox);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.urlComboBox);
      this.Controls.Add((Control) this.listControlAreasButton);
      this.Controls.Add((Control) this.helpButton);
      this.Controls.Add((Control) this.passwordTextBox);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.zoneCodeTextBox);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.userIdTextBox);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.listBox1);
      this.Controls.Add((Control) this.queryByZoneButton);
      this.Margin = new Padding(4, 4, 4, 4);
      this.Name = nameof (Form1);
      this.Text = "Parkeon Sandbox";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
