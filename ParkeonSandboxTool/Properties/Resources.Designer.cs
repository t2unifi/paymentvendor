﻿// Decompiled with JetBrains decompiler
// Type: ParkeonSandboxTool.Properties.Resources
// Assembly: ParkeonSandboxTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6172E00A-FD81-4175-997F-5AC23D56F817
// Assembly location: C:\SWAN\MEA\Parkeon Sandbox Tool 2014 06 30\ParkeonSandboxTool\ParkeonSandboxTool.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace ParkeonSandboxTool.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  [CompilerGenerated]
  [DebuggerNonUserCode]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (ParkeonSandboxTool.Properties.Resources.resourceMan == null)
          ParkeonSandboxTool.Properties.Resources.resourceMan = new ResourceManager("ParkeonSandboxTool.Properties.Resources", typeof (ParkeonSandboxTool.Properties.Resources).Assembly);
        return ParkeonSandboxTool.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return ParkeonSandboxTool.Properties.Resources.resourceCulture;
      }
      set
      {
        ParkeonSandboxTool.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
